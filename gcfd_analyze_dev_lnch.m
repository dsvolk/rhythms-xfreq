%% Generalized Cross-Frequency Decomposition (realistic, MNI-based) Head Simulation script
%% Denis VOLK, Centre for Cognition & Decision Making, National Research University Higher School of Economics, Moscow, Russia

%% TODO:
%  time delays, in addition to phase delays
%  iteratively project out found components
%  ---
%  ideas:
%  iterative search for filter_p, filter_q
%  how to seed with a harmonic while secondary_ssd_on ?

warning('on','MATLAB:RandStream:ActivatingLegacyGenerators')
warning('on','MATLAB:RandStream:ReadingInactiveLegacyGeneratorState') 

%% Setup the environment
warning('off','MATLAB:dispatcher:pathWarning'); % suppress warnings about non-existent folders

addpath(genpath('..'));         % add all same-level-directories to MATLAB search path

addpath('/Users/1/MATLAB_Toolboxes/Guido_toolbox/');    % for Volkobook
addpath('/Users/1/MATLAB_Toolboxes/bbci_public/');      % Berlin BCI for Volkobook
%addpath('/Users/1/MATLAB_Toolboxes/eeglab13_6_5b/');    % EEGLab for Volkobook
%addpath('/Users/1/MATLAB_Data/Zafer_Rest17');
addpath('/Users/1/Documents/!Docs/HSE-CDM-DATA/2017-04-10 Alexey Gorin');
addpath('/Users/1/Documents/!Docs/HSE-CDM-DATA/2017-04-10 Alexey Gorin');

addpath('C:/MATLAB_Toolboxes/Guido_toolbox/');   % for Denis's Windows machines
addpath('C:/MATLAB_Toolboxes/bbci_public/');     % Berlin BCI for Denis's Windows machines
%addpath('C:/MATLAB_Data/Zafer_Rest17');

% Sasha, Daniil, please add your path strings here

warning('on','MATLAB:dispatcher:pathWarning');

startup_bbci_toolbox;

output_dir = ['./out_analyze_dev/' datestr(now,'yymmdd_HHMMSS') '/'];
mkdir(output_dir);

%% Preparing to start the script
diary([output_dir 'diary.txt']);
close all;
clearvars -except 'rng_saved_states' 'output_dir';
disp('================================================================================');
disp('==== gCFD Analyze EEG signals for Synchronized Rhythms =========================');
disp('================================================================================');
global PIDX;    % iterator over various parameter settings
global TIDX;    % iterator over trials within each PIDX
rng('shuffle','twister');
rng_seed_default = rng();

stdout([datestr(now,'dd/mm/yyyy') '. Ready to rock now!']);
disp('[Developer/debug launcher]');
ScriptCPUStart = cputime;

%% Set up the parameters
gcfd_analyze_dev_ctrl;

%% Main script
gcfd_analyze_batch;

%% Finishing the script
ScriptCPUFinish = cputime;
stdout(['Finished, taking ' sprintf('%.2f',ScriptCPUFinish-ScriptCPUStart) ' secs of CPU time.']);
diary off;

%% Play a sound
if sound_on_finish
    [snd,Fs] = audioread('batch_script_complete.m4a');
    sound(snd,Fs);
end

% email_denis(['[MATLAB] gcfd_head_sim.m has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.'],['The script "gcfd_head_sim.m" has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.']);
return;

