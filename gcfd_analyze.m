function [fig_idx] = gcfd_analyze ( elec, epochs, butter_p_b, butter_p_a, butter_q_b, butter_q_a, GoodMnt2D, param, ...
        fig_handles, fig_idx, plotting_on )
    
unpack_param(param, 'param');

if FwdModel ~= FwdModelType.Data
    batcherr('gcfd_analyze is for FwdModel == FwdModelType.Data only.');
    return;
end

%% Main part
if ssd_primary_on
    batchout('SSD for FrP components...');
    %% run SSD to recover FrP sources
    [W, pattern_p, lambda, C_s, X_ssd_p] = ssd(elec, [FrP*FrBase-1 FrP*FrBase+1; FrP*FrBase-3 FrP*FrBase+3; FrP*FrBase-2 FrP*FrBase+2], Discr, 2, epochs);
    batchout('Done.');
    
    %% save the unfiltered components
    component_prim_ssd = elec * W(:,1:PairsNum);
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrP*FrBase, lambda, PairsNum, ssd_secondary_comp_num );
        end
        if plot_ssd_topo
            [fig_idx] = plot_topo_ssd_func ( fig_handles, fig_idx, output_dir, FrP*FrBase, lambda, pattern_p, PairsNum, GoodMnt2D );
        end
    end   
    
else
    batcherr('SSD is off but this makes no sense in FwdModelType.Data mode.');
    return;
end

if ssd_secondary_on
    batchout('SSD for FrQ components...');
    %% run SSD to recover FrQ sources
    [W_q, pattern_q_ssd, lambda, C_s, X_ssd_q] = ssd(elec, [FrQ*FrBase-1 FrQ*FrBase+1; FrQ*FrBase-3 FrQ*FrBase+3; FrQ*FrBase-2 FrQ*FrBase+2], Discr, 2, epochs);
    batchout('Done.');
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrQ*FrBase, lambda, PairsNum, ssd_secondary_comp_num );
        end
        if plot_ssd_topo
            [fig_idx] = plot_topo_ssd_func ( fig_handles, fig_idx, output_dir, FrQ*FrBase, lambda, pattern_q_ssd, PairsNum, GoodMnt2D );
        end
    end
else
    batchout('SSD for FrQ is off. Running gCFD on raw channels.');
    %pattern_p = pattern_sig_p;
    %X_ssd_p = src_sig_p;
end

batchout('gCFD...');
%% genCFD DECOMPOSITION PART:
%% prepare the signals
gcfd_elec_q_num = NaN;
if ssd_secondary_on
    gcfd_elec_q_num = min([size(X_ssd_q,2) ssd_secondary_comp_num]);
    gcfd_elec_q = X_ssd_q(:,1:gcfd_elec_q_num);
else
    gcfd_elec_q_num = size(elec,2);
    gcfd_elec_q = filtfilt(butter_q_b,butter_q_a,elec);
end

gcfd_elec_q_num
filter_q = NaN(gcfd_elec_q_num,PairsNum);

gcfd_elec_q = detrend(gcfd_elec_q,'constant');
X_ssd_p = detrend(X_ssd_p,'constant');

%% complexify the signals (compute 'analytic signals')
m = hilbert(gcfd_elec_q);
r = hilbert(X_ssd_p);

%% frequency warp the SSD component
if targets_abs_1
    r = exp(1i.*(FrQ*angle(r) + experimenal_freq_warp_offset));
else
    r = abs(r).*exp(1i.*(FrQ*angle(r) + experimenal_freq_warp_offset));
end

%% filter the warped components
if post_warp_pq_filter_on
    r = hilbert(filtfilt(butter_pq_b,butter_pq_a,real(r)));
end

%% Linear fit from the orginal paper (Nikulin et al, "Cross-Frequency Decomposition...", 2012)
if FrP == 1
    batchout(sprintf('Linear least-squares optimization from CFD''2012''...'));
    [filter_q_vadim] = cfd2012_fit ( gcfd_elec_q, real(r) );
    batchout(sprintf('Done.'));

    %% Filter-to-pattern
    for idx = 1:PairsNum
        pattern_q_vadim_cfd(1:gcfd_elec_q_num,idx) = filter2pattern(filter_q_vadim(:,idx), gcfd_elec_q);
    end
end

%% Prepare the seeds for the nonlinear optimization
f0_default = ones(gcfd_elec_q_num,PairsNum)/gcfd_elec_q_num;

if filter_seed_cfd2012
    assert(FrP == 1);
    f0_default = filter_q_vadim;
end

if filter_seed_ssd
    assert (ssd_primary_on, 'filter_seed_ssd == true is not compatible with ssd_primary_on == false');
    assert (~ssd_secondary_on, 'filter_seed_ssd == true is not compatible with ssd_secondary_on == true');
    
    elec_p = filtfilt(butter_p_b,butter_p_a,elec);
    cov_p = elec_p' * elec_p;
    elec_q = filtfilt(butter_q_b,butter_q_a,elec);
    cov_q = elec_q' * elec_q;
    
    f0_default = cov_q \ cov_p * W(:,1:PairsNum);
end

if ~randomize_filter_seeds
    filter_seeds_num = 1;   % no need to have more than one same seed
end

%% inject a head topo plot
if lsqnonlin_inject_topo_plot
    assert(~ssd_secondary_on, 'inject_head_topo_plot == true only works with ssd_secondary_on == false');
    lsqnonlin_param.PlotFcn{end+1} = @(x,optimValues,state,varargin) lsqnonlin_plot_topo(x,optimValues,state,varargin, gcfd_elec_q, GoodMnt2D);
end

%% Nonlinear optimization
if project_out_found_sources
    gcfd_elec_q_ortho = gcfd_elec_q;
    
    idx = 1;
    batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
    filter_q_ortho{idx} = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
    inv_transform_all{idx} = eye(gcfd_elec_q_num);
    
    for idx = 2:PairsNum
        [gcfd_elec_q_ortho, inv_transform_one_step{idx}] = project_to_ortho ( gcfd_elec_q_ortho, filter_q_ortho{idx-1} );
        batchout(sprintf('[%d/%d] Projected out the gCFD source for SSD component %d.', idx-1, PairsNum, idx-1));
        
        inv_transform_all{idx} = inv_transform_all{idx-1} * inv_transform_one_step{idx};
        m = hilbert(gcfd_elec_q_ortho);        
                
        batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
        filter_q_ortho{idx} = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
    end
    
    for idx = 1:PairsNum
        filter_q(:,idx) = inv_transform_all{idx} * filter_q_ortho{idx};
    end       
else
    for idx = 1:PairsNum
        %% for each of FrP sources...
        batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
        filter_q(:,idx) = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
        batchout(sprintf('Filter %d:', idx));
        disp(filter_q(:,idx));
    end
end

%% Save the unfiltered components
if ssd_secondary_on    
    W_q_reduced = W_q (:,1:size(filter_q,1));
else    
    W_q_reduced = ones(size(filter_q,1));
end
component_gcfd = elec * W_q_reduced * filter_q;

%% Filter-to-pattern
clear pattern_q_gcfd
pattern_q_gcfd = NaN(gcfd_elec_q_num,PairsNum); 
for idx = 1:PairsNum
    pattern_q_gcfd(1:gcfd_elec_q_num,idx) = filter2pattern (filter_q(:,idx), gcfd_elec_q);
end

if ssd_secondary_on
    %% Convert the pattern in SSD coordinates to the pattern in original coordinates
    pattern_q_gcfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
    pattern_q = pattern_q_ssd * pattern_q_gcfd;
    
    if FrP == 1
        pattern_q_vadim_cfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
        pattern_q_vadim = pattern_q_ssd * pattern_q_vadim_cfd;
    end
else
    pattern_q = pattern_q_gcfd;
    if FrP == 1
        pattern_q_vadim = pattern_q_vadim_cfd;
    end
end

%% Display results

[fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
fig.Color = 'white';
colormap('jet');

if source_topo_figures_normalize_ranges_across_scalps
    fig.Name = sprintf('[P=%d] Fig. %d: Recovered %dHz sources (same scale across rows)', cur_param_idx, fig_idx, FrQ*FrBase);
else
    fig.Name = sprintf('[P=%d] Fig. %d: Recovered %dHz sources', cur_param_idx, fig_idx, FrQ*FrBase);
end

rangescalp_p_abs = max(max(abs(pattern_p(:,1:PairsNum))));
rangescalp_p = [-rangescalp_p_abs rangescalp_p_abs];

if FrP == 1
    rangescalp_q_vadim_abs = max(max(abs(pattern_q_vadim(:,1:PairsNum))));
    rangescalp_q_vadim = [-rangescalp_q_vadim_abs rangescalp_q_vadim_abs];
end

rangescalp_q_abs = max(max(abs(pattern_q(:,1:PairsNum))));
rangescalp_q = [-rangescalp_q_abs rangescalp_q_abs];

%%
for n=1:PairsNum
    %subplot(3,PairsNum,n);   % Top: SSD recovered alpha sources
    subplot(3,PairsNum,n);
    ppp = pattern_p(:,n);
    if source_topo_figures_normalize_ranges_across_scalps
        ppp_range = rangescalp_p;
    else
        ppp_range = [-max(abs(ppp(:))), max(abs(ppp(:)))];
    end
    
    %%
    plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', ppp_range, 'Shading', 'interp', 'Extrapolation', 1);
    %%
    if ssd_primary_on
        title(sprintf('SSD %dHz #%d', FrP*FrBase, n));
    else
        title(sprintf('True %dHz #%d', FrP*FrBase, n));
    end
    
%     subplot(3,PairsNum,n+PairsNum);   % Middle: CFD'2012 recovered beta sources (just for reference)
%     qqq = pattern_q_vadim(:,n);
%     plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', rangescalp_q_cfd, 'Shading', 'interp', 'Extrapolation', 1);
%     title(sprintf('CFD''2012 %dHz #%d', FrQ*FrBase, n));
    
      
    %subplot(3,PairsNum,n+2*PairsNum);   % Bottom: gCFD recovered beta sources
    subplot(3,PairsNum,n+PairsNum);
    qqq = pattern_q(:,n);
    if source_topo_figures_normalize_ranges_across_scalps
        qqq_range = rangescalp_q;
    else
        qqq_range = [-max(abs(qqq(:))), max(abs(qqq(:)))];
    end
    %%
    plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', qqq_range, 'Shading', 'interp', 'Extrapolation', 1);
    %%    
    if calc_sync_measures
        sig_q(:,n) = gcfd_elec_q * filter_q(:,n);    % the de-mixed component
        kappa2 = kuramoto ( X_ssd_p(:,n), sig_q(:,n), FrP, FrQ );  % sync measure between the SSD component at FrP and the de-mixed component at FrQ
        
        pat_diff = vec_mismatch (ppp,qqq);
              
        str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{p-q}', kappa2, '\angle', pat_diff);
    else
        str_sync_measures = '';
    end
    
    %%
    title(sprintf('gCFD %dHz #%d%s', FrQ*FrBase, n, str_sync_measures));
    
    if calc_sync_measures
        subplot(3,PairsNum,n+2*PairsNum);   % Bottom: spectrums
        
        %%
        %figure; 
        hold on; grid on;
        
        %limit_hertz = FrBase * (max(FrP,FrQ) + 1);
        
        [pxx,f] = pwelch(component_prim_ssd(:,n),Discr,[],[],Discr);
        
        fr_mask = (fr_cutoff_low < f) & (f < fr_cutoff_high);
        %fr_mask([1 2]) = 0;     
        
        pxx = pxx / sum(pxx(fr_mask));       
        %[pxx,f] = pwelch(component_p, 10*Discr, [], 2048, Discr);
        plot(f,10*log10(pxx),'LineWidth',2); 
        %plot(f,pxx,'LineWidth',2); 
        
        [pxx,f] = pwelch(component_gcfd(:,n),Discr,[],[],Discr);
        pxx = pxx / sum(pxx(fr_mask));        
        %[pxx,f] = pwelch(component_q, 10*Discr, [], 2048, Discr);
        plot(f,10*log10(pxx),'LineWidth',2);
        %plot(f,pxx,'LineWidth',2); 
        
        legend('SSD', 'gCFD');
        xlim([fr_cutoff_low fr_cutoff_high]);
        
        plot([FrBase*FrP FrBase*FrP], ylim, '--k');
        plot([FrBase*FrQ FrBase*FrQ], ylim, '--k');
        
        xlabel('Frequency (Hz)');
        ylabel('Magnitude (dB)');        
        
        hold off;
    end
    
end

%% research component self-sync on different frequencies
%% primary SSD components
component_prim_ssd_p = filtfilt(butter_p_b,butter_p_a,component_prim_ssd);
component_prim_ssd_q = filtfilt(butter_q_b,butter_q_a,component_prim_ssd);

%% final recovered components
component_gcfd_p = filtfilt(butter_p_b,butter_p_a,component_gcfd);
component_gcfd_q = filtfilt(butter_q_b,butter_q_a,component_gcfd);

%%
if calc_sync_measures
%     [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
%     fig.Color = 'white';
%     colormap('jet');
    
    kappa_all = NaN(PairsNum+2, PairsNum+2);
    corr_all = NaN(PairsNum+2, PairsNum+2);

    for i=1:PairsNum
        for j=1:PairsNum
            kappa_all(i,j) = kuramoto ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ );
            corr_all(i,j) = sync_index_corr ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ, Discr/FrBase/2 );
        end
    end
    
    batchout('Primary SSD components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(i,end) = kuramoto ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ );
        corr_all(i,end) = sync_index_corr ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ, Discr/FrBase/2 );   
    end
    batchout('Final gCFD recovered components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(end,i) = kuramoto ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ );
        corr_all(end,i) = sync_index_corr ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ, Discr/FrBase/2 );
    end
    
    %%
    filename = sprintf('[P=%d] Sync Indices', cur_param_idx);
    figure('name',filename,'NumberTitle','off');
    
    pcolor([kappa_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);

    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3; 
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3; 
    ax.YTickLabel = {'1','2','3','4','5','','harmo'}; 
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(kappa_all(:))]); colormap('summer');
    hold off;
    %savefig([output_dir filename]);
       
    %%    
    filename = sprintf('[P=%d] Correlations', cur_param_idx);
    figure('name',filename,'NumberTitle','off');
    
    pcolor([corr_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);
    
    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3; 
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3; 
    ax.YTickLabel = {'1','2','3','4','5','','harmo'}; 
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(corr_all(:))]); colormap('autumn');
    hold off;
    %savefig([output_dir filename]);
    
    %%
    batchout('Cross- and self-sync on different frequencies');
    kappa_all
    batchout('Cross- and self-correlations on different frequencies');
    corr_all
end

%% Show the channel labels
%figure; plot_scalp(GoodMnt2D, zeros(size(GoodMnt2D.x)), 'ShowLabels', 1, 'CLim', [-1,1], 'Shading', 'interp', 'Extrapolation', 1);

end
