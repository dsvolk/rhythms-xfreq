function [PRE_p_final, PRE_q_final, fig_idx] = gcfd_duo_test ...
    ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
    butter_p_b, butter_p_a, butter_q_b, butter_q_a, ...
    source_analysis, GoodMnt2D, param, ...
    fig_handles, fig_idx, output_dir, plotting_on )

unpack_param(param, 'param');

assert ((FwdModel == FwdModelType.Rand) || (FwdModel == FwdModelType.GuidoMNI), 'Only ''FwdModelType.Rand'' and ''FwdModelType.GuidoMNI'' are supported.\nUse ''gcfd_analyze.m'' for what used to be ''FwdModelType.Data''');

if ssd_secondary_on
        batchout('SSD for FrP components...');
    %% SSD
    [W_p, pattern_p_ssd, lambda_p, C_s, X_ssd_p] = ssd(elec, [FrP*FrBase-1 FrP*FrBase+1; FrP*FrBase-3 FrP*FrBase+3; FrP*FrBase-2 FrP*FrBase+2], Discr, 2, [1 T]);
    batchout('Done.');
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrP*FrBase, lambda_p, PairsNum, ssd_secondary_comp_num );
        end
    end
    
    batchout('SSD for FrQ components...');
    %% run SSD to recover FrQ sources
    [W_q, pattern_q_ssd, lambda_q, C_s, X_ssd_q] = ssd(elec, [FrQ*FrBase-1 FrQ*FrBase+1; FrQ*FrBase-3 FrQ*FrBase+3; FrQ*FrBase-2 FrQ*FrBase+2], Discr, 2, [1 T]);
    batchout('Done.');
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrQ*FrBase, lambda_q, PairsNum, ssd_secondary_comp_num );
        end
    end
else
    batchout('SSD is off. Running GCFD Duo on raw channels.');
end

batchout('gCFD...');
%% genCFD DECOMPOSITION PART:
%% prepare the signals
gcfd_elec_q_num = NaN;
if ssd_secondary_on
    gcfd_elec_p_num = min([size(X_ssd_p,2) ssd_secondary_comp_num]);
    gcfd_elec_p = X_ssd_p(:,1:gcfd_elec_p_num);
    
    gcfd_elec_q_num = min([size(X_ssd_q,2) ssd_secondary_comp_num]);
    gcfd_elec_q = X_ssd_q(:,1:gcfd_elec_q_num);            
else
    gcfd_elec_p_num = size(elec,2);
    gcfd_elec_p = filtfilt(butter_p_b,butter_p_a,elec);
    
    gcfd_elec_q_num = size(elec,2);
    gcfd_elec_q = filtfilt(butter_q_b,butter_q_a,elec);
end

gcfd_elec_p_num
filter_p = NaN(gcfd_elec_p_num,PairsNum);
gcfd_elec_q_num
filter_q = NaN(gcfd_elec_q_num,PairsNum);

gcfd_elec_p = detrend(gcfd_elec_p,'constant');
gcfd_elec_q = detrend(gcfd_elec_q,'constant');

%% complexify the signals (compute 'analytic signals')
mp = hilbert(gcfd_elec_p);
mq = hilbert(gcfd_elec_q);

%% Prepare the seeds for the nonlinear optimization
f0p_default = ones(gcfd_elec_p_num);
f0q_default = ones(gcfd_elec_q_num);

if ~randomize_filter_seeds
    filter_seeds_num = 1;   % no need to have more than one same seed
end

%% inject a head topo plot
if lsqnonlin_inject_topo_plot
    %% probably not working in this version
    assert(false);
    assert(~ssd_secondary_on, 'inject_head_topo_plot == true only works with ssd_secondary_on == false');
    lsqnonlin_param.PlotFcn{end+1} = @(x,optimValues,state,varargin) lsqnonlin_plot_topo(x,optimValues,state,varargin, gcfd_elec_q, GoodMnt2D);
end

%% Nonlinear optimization
[filter_p, filter_q] = gcfd_duo_optimize ( mp, mq, FrP, FrQ, randomize_filter_seeds, filter_seeds_num, lsqnonlin_param );

%% Save the unfiltered components
if ssd_secondary_on    
    W_p_reduced = W_p (:,1:size(filter_p,1));
    W_q_reduced = W_q (:,1:size(filter_q,1));
else
    W_p_reduced = ones(size(filter_p,1));
    W_q_reduced = ones(size(filter_q,1));
end
component_gcfd_p = elec * W_p_reduced * filter_p;
component_gcfd_q = elec * W_q_reduced * filter_q;

%% Filter-to-pattern
% clear pattern_q_gcfd
% for idx = 1:PairsNum
%     pattern_q_gcfd(1:gcfd_elec_q_num,idx) = filter2pattern (filter_q(:,idx), gcfd_elec_q);
% end

pattern_p_gcfd(1:gcfd_elec_p_num) = filter2pattern (filter_p(:), gcfd_elec_p);
pattern_q_gcfd(1:gcfd_elec_q_num) = filter2pattern (filter_q(:), gcfd_elec_q);

if ssd_secondary_on
    %% Convert the patterns in SSD coordinates to the patterns in original coordinates
    pattern_p_gcfd (gcfd_elec_p_num+1:size(X_ssd_p,2),:) = 0;
    pattern_p = pattern_p_ssd * pattern_p_gcfd;
    
    pattern_q_gcfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
    pattern_q = pattern_q_ssd * pattern_q_gcfd;
else
    pattern_p = pattern_p_gcfd;   
    pattern_q = pattern_q_gcfd;   
end

%%
pattern_p = pattern_p';
pattern_q = pattern_q';

%%
[ pat_sig_p_idx, pat_p_idx, PRE_p_final ] = match_patterns_Volk2 ( pattern_sig_p, pattern_p, 1 );
[ pat_sig_q_idx, pat_q_idx, PRE_q_final ] = match_patterns_Volk2 ( pattern_sig_q, pattern_q, 1 );

batchout(sprintf('gCFD pattern recovery error for P:'));
disp(PRE_p_final);
batchout(sprintf('gCFD pattern recovery error for Q:'));
disp(PRE_q_final);

if FwdModel == FwdModelType.GuidoMNI  
    if source_topo_figures
        %% plotting recovered FrP sources
        [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
        fig.Color = 'white';
        colormap('jet');
        fig.Name = sprintf('Fig. %d: Recovered %dHz sources', fig_idx, FrP*FrBase);
        filename = sprintf('[Fig %d] Recovered %dHz sources', fig_idx, FrP*FrBase);

        rowsNum = 2;
        
        %for n=1:PairsNum    
        n = 1;
            %% Top: true beta sources
            subplot(rowsNum,PairsNum,n);   
            title(sprintf('True %dHz #%d', FrP*FrBase, pat_sig_p_idx(n)));
            showfield(pattern_sig_p(:,pat_sig_p_idx(n)), source_analysis.locs_2D);            
            
            %% Bottom: gCFD recovered beta sources
            subplot(rowsNum,PairsNum,n+(rowsNum-1)*PairsNum);  
            
            if calc_sync_measures
                sig = gcfd_elec_p * filter_p(:,pat_p_idx(n));    % the de-mixed component
                kappa1 = kuramoto ( src_sig_p(:,pat_sig_p_idx(n)), sig, 1, 1 );  % sync measure between the true signal and the gCFD de-mixed component at FrQ
                %kappa2 = kuramoto ( X_ssd_p(:,pat_q_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD component at FrP and the gCFD de-mixed component at FrQ
                kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the gCFD de-mixed component at FrQ
                str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
            else
                str_sync_measures = '';
            end
            
            title(sprintf('gCFD %dHz #%d\nError = %.4f%s', FrP*FrBase, pat_p_idx(n),  PRE_p_final(n), str_sync_measures));
            showfield(pattern_p(:,pat_p_idx(n)), source_analysis.locs_2D);
        %end
        
        drawnow;
        savefig([output_dir filename]);
        
        %% plotting recovered FrQ sources
        [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
        fig.Color = 'white';
        colormap('jet');
        fig.Name = sprintf('Fig. %d: Recovered %dHz sources', fig_idx, FrQ*FrBase);
        filename = sprintf('[Fig %d] Recovered %dHz sources', fig_idx, FrQ*FrBase);
        rowsNum = 2;
        
        %for n=1:PairsNum    
        n = 1;     
            %% Top: true beta sources
            subplot(rowsNum,PairsNum,n);   
            title(sprintf('True %dHz #%d', FrQ*FrBase, pat_sig_q_idx(n)));
            showfield(pattern_sig_q(:,pat_sig_q_idx(n)), source_analysis.locs_2D);
            
            %% Bottom: gCFD recovered beta sources
            subplot(rowsNum,PairsNum,n+(rowsNum-1)*PairsNum);  
            
            if calc_sync_measures
                sig = gcfd_elec_q * filter_q(:,pat_q_idx(n));    % the de-mixed component
                kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_idx(n)), sig, 1, 1 );  % sync measure between the true signal and the gCFD de-mixed component at FrQ
                %kappa2 = kuramoto ( X_ssd_p(:,pat_q_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD component at FrP and the gCFD de-mixed component at FrQ
                kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the gCFD de-mixed component at FrQ
                str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
            else
                str_sync_measures = '';
            end
            
            title(sprintf('gCFD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_idx(n),  PRE_q_final(n), str_sync_measures));
            showfield(pattern_q(:,pat_q_idx(n)), source_analysis.locs_2D);
        %end
        
        drawnow;
        savefig([output_dir filename]);
    end
end

%% these figures are temporary disabled - no time to debug
if false
    
    %% research component self-sync on different frequencies
    %% primary SSD components
    if ssd_primary_on
        component_prim_ssd_p = filtfilt(butter_p_b,butter_p_a,component_prim_ssd);
        component_prim_ssd_q = filtfilt(butter_q_b,butter_q_a,component_prim_ssd);
    end
    
    %% final recovered components
    component_gcfd_p = filtfilt(butter_p_b,butter_p_a,component_gcfd);
    component_gcfd_q = filtfilt(butter_q_b,butter_q_a,component_gcfd);
    
    
    %     [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
    %     fig.Color = 'white';
    %     colormap('jet');
    
    kappa_all = NaN(PairsNum+2, PairsNum+2);
    corr_all = NaN(PairsNum+2, PairsNum+2);
    
    for i=1:PairsNum
        for j=1:PairsNum
            kappa_all(i,j) = kuramoto ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ );
            corr_all(i,j) = sync_index_corr ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ, Discr/FrBase/2 );
        end
    end
    
    batchout('Primary SSD components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(i,end) = kuramoto ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ );
        corr_all(i,end) = sync_index_corr ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ, Discr/FrBase/2 );
    end
    batchout('Final gCFD recovered components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(end,i) = kuramoto ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ );
        corr_all(end,i) = sync_index_corr ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ, Discr/FrBase/2 );
    end
    
    %%
    filename = sprintf('Sync Indices');
    figure('name',filename,'NumberTitle','off');
    
    pcolor([kappa_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);
    
    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3;
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3;
    ax.YTickLabel = {'1','2','3','4','5','','harmo'};
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(kappa_all(:))]); colormap('summer');
    hold off;
    %savefig([output_dir filename]);
    
    %%
    filename = sprintf('Correlations');
    figure('name',filename,'NumberTitle','off');
    
    pcolor([corr_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);
    
    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3;
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3;
    ax.YTickLabel = {'1','2','3','4','5','','harmo'};
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(corr_all(:))]); colormap('autumn');
    hold off;
    %savefig([output_dir filename]);
    
    %%
    batchout('Cross- and self-sync on different frequencies');
    kappa_all
    batchout('Cross- and self-correlations on different frequencies');
    corr_all
end

%% Show the channel labels
%figure; plot_scalp(GoodMnt2D, zeros(size(GoodMnt2D.x)), 'ShowLabels', 1, 'CLim', [-1,1], 'Shading', 'interp', 'Extrapolation', 1);


