function [idx] = cell_idx (cell_array, string_list)

cellfind = @(string)(@(cell_contents)(ismember(cell_contents,string)));
idx = cellfun(cellfind(string_list), cell_array);