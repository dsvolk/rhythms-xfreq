function [elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, seeds] = mk_elec ( source_analysis, GoodMnt2D, param, ...
    butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a )

unpack_param(param, 'param');

%% Make random mixing patterns based on a head model
switch FwdModel
    case FwdModelType.GuidoMNI
        assert(seed_pat_sig == 0 && seed_pat_noise == 0);
        if seed_pat_all == 0
            rng('shuffle');
            seed_pat_all = randi([1 9999999])
        else
            seed_pat_all
        end
        seeds.seed_pat_all = seed_pat_all;
        rng(seed_pat_all,'twister');
        [pattern_sig, pattern_noise] = mk_model_mni (source_analysis, SourceNum, NoiseNum);
        
    case FwdModelType.Rand
        % if we use seed_pat_sig and seed_pat_noise we don't use
        % seed_pat_all
        if seed_pat_sig ~= 0 && seed_pat_noise ~= 0
            seed_pat_sig
            rng(seed_pat_sig,'twister');
            pattern_sig = right_stoch_matrix (SensorsNum, SourceNum);
            seed_pat_noise
            rng(seed_pat_noise,'twister');
            pattern_noise = right_stoch_matrix (SensorsNum, NoiseNum);
        else
            if seed_pat_all == 0
                rng('shuffle');
                seed_pat_all = randi([1 9999999])
            else
                seed_pat_all
            end
            rng(seed_pat_all,'twister');
            pattern_sig = right_stoch_matrix (SensorsNum, SourceNum);
            pattern_noise = right_stoch_matrix (SensorsNum, NoiseNum);
        end
    case FwdModelType.Data
        batchout('Real data mode.'); % pattern is unknown
    otherwise
        batcherr(['Unexpected value of FwdModel parameter, ''' FwdModel '''']);
        return;
end

%% 
po = pattern_overlap (pattern_sig);
batchout('Mixing patterns generated.');
batchout(sprintf('Median vec_mismatch = %.4f, min = %.4f.\n', nanmedian(po(:)), nanmin(po(:)))); % pattern is unknown

%%
if exist('pattern_sig','var')
    pattern_sig_p = pattern_sig(:,1:PairsNum);
    pattern_sig_q = pattern_sig(:,PairsNum+1:SourceNum);
end

%%
switch FwdModel
    case FwdModelType.Data  % if we are using real data.. load the data
        eeg = load('BLINKS');
        elec = eeg.F';
        elec = elec(:,GoodChannels);
        
    case {FwdModelType.GuidoMNI, FwdModelType.Rand}                       % if we are using simulated data.. prepare the data
        %% generate # = PairsNum synchronized pairs
        if seed_src_sig == 0
            rng('shuffle');
            seed_src_sig = randi([1 9999999])
        else
            seed_src_sig
        end
        seeds.seed_src_sig = seed_src_sig;
        rng(seed_src_sig,'twister');
        [src_sig_p, src_sig_q] = mk_sync_sig (T, PairsNum, butter_base_b, butter_base_a, FrP, FrQ, ampli_flip, normalize_sources, 0, 0);
        
        %% prepare and mix the noise
        if seed_src_noise == 0
            rng('shuffle');
            seed_src_noise = randi([1 9999999])
        else
            seed_src_noise
        end
        seeds.seed_src_noise = seed_src_noise;
        rng(seed_src_noise,'twister');
        src_noise = mk_pink_noise(T,NoiseNum);
        elec_noise = src_noise * pattern_noise';
        
        elec_noise_p = filtfilt(butter_p_b,butter_p_a,elec_noise);
        elec_noise_q = filtfilt(butter_q_b,butter_q_a,elec_noise);
        
        var_elec_noise_p = sum(var(elec_noise_p));
        var_elec_noise_q = sum(var(elec_noise_q));
        
        %% adjust SNR if necessary
        [src_sig_p, src_sig_q] = snr_adjust (PairsNum, SNR, src_sig_p, src_sig_q, pattern_sig, var_elec_noise_p, var_elec_noise_q, compute_snr, normalize_snr);
        
        %% mix the sources (Src) into channels
        src_sig = [src_sig_p src_sig_q];
        elec_sig = src_sig * pattern_sig';
        
        %% Finally, we have signals in the sensors space:
        elec = elec_sig + elec_noise;
        
        %% END of PREPARING the DATA (real or simulated)
end

end