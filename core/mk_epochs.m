function [good_sectors, bad_sectors] = mk_epochs (raw_data, sample_limit)
% In:   raw_data structure 
% Out:  good_sectors (epochs)
%       bad_sectors (gaps between the good sectors)

bad_sectors = [];
good_sectors = [];

for i = 1:length(raw_data.Events)
    
   if strcmp(raw_data.Events(i).label, 'BAD')== 1
        bad_sectors = raw_data.Events(i).samples;
        break       
   end
    
end

bad = [];

if isempty(bad_sectors)
    good_sectors = [1 length(raw_data.F)];
else
    if bad_sectors(1) ~= 0
        bad(1,:) = [bad_sectors(1,:) length(raw_data.F)];
        bad(2,:) = [1  bad_sectors(2,:)];   
    else
        bad(1,:) = [bad_sectors(1,2:end) length(raw_data.F)];
        bad(2,:) = bad_sectors(2,:);   
    end
    bad = bad';
    good_sectors(:,1) = bad(:,2);
    good_sectors(:,2) = bad(:,1);

    good_sectors(find(good_sectors(:, 2) - good_sectors(:,1) < sample_limit), :) = [];
end