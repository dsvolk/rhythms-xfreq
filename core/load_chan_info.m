function [source_analysis, GoodMnt2D, GoodChannels] = load_chan_info (param)
%% Never tested in FwdModelType.Data or FwdModelType.Rand mode, bugs are possible

switch param.FwdModel
    case FwdModelType.GuidoMNI
        source_analysis = prepare_sourceanalysis(param.ElecList,'montreal');
        electrodes_num = size(source_analysis.clab_electrodes,2);
        electrodes_num

        GoodChannels = [1:electrodes_num];
        
        %[source_analysis, GoodChannels] = load_sourceanalysis_mni ();
        GoodMnt2D = [];
        
    case FwdModelType.Rand
        source_analysis = [];
        GoodChannels = [1:param.SensorsNum];
        GoodMnt2D = [];
        
    case FwdModelType.Data
        source_analysis = [];
        [GoodMnt2D, GoodChannels] = load_mnt_channels ();
        %default.SensorsNum = sum(GoodChannels);
end

end