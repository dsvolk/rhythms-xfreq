function [pattern] = filter2pattern_cov (filter, e_cov)
% Convert a spatial filter to a spatial pattern based on formula (19) from 
% Nikulin, Nolte, Curio "Cross-frequency decomposition: A novel technique for studying interactions between neuronal oscillations with different frequencies", Clinical Neurophysiology, 2012
% who, in turn, cite
% Parra LC, Spence CD, Gerson AD, Sajda P. "Recipes for the linear analysis of EEG" Neuroimage 2005;28:326?41
% 
% INPUT: 
%     filter - spatial filter (demixing coefficients), column vector of size N
%     e_cov  - covariation of electrode signals, e_cov = elec' * elec, matrix of size NxN
%
% OUTPUT:
%     pattern - spatial pattern (mixing coefficients), column vector of size N

pattern = (e_cov * filter) / (filter' * e_cov * filter);

end