% function [GoodChannels] = load_mnt_channels_bbci (dcc)
% % dcc = data.channels.Channel
% 
% for i = 1:length(dcc)
%     GoodChannels(i) = strcmp (dcc(i).Type, {'EEG'});
% end
% 
% 
% end


function [GoodMnt2D, GoodChannels] = load_mnt_channels_bbci (dcc, Mnt2D, bad_channels)
% dcc = data.channels.Channel

GoodChannels = NaN(size(dcc));
for i = 1:length(dcc)
    GoodChannels(i) = strcmp (dcc(i).Type, {'EEG'});
end

BadChannels = cell_idx (Mnt2D.clab', bad_channels);
GoodChannels = GoodChannels & ~BadChannels'; 

%%
GoodMnt2D = struct;
GoodMnt2D.x = Mnt2D.x(GoodChannels);
GoodMnt2D.y = Mnt2D.y(GoodChannels);
GoodMnt2D.pos_3d = Mnt2D.pos_3d(:,GoodChannels);
GoodMnt2D.clab = Mnt2D.clab(:,GoodChannels);

end