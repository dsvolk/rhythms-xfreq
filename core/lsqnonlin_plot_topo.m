%function stop = lsqnonlin_plot_topo(x,optimValues,state,varargin, e_cov, GoodMnt2D)
function stop = lsqnonlin_plot_topo(x,optimValues,state,varargin, elec, GoodMnt2D)
% OPTIMPLOTRESNORM Plot value of the norm of residuals at each iteration.
%
%   STOP = OPTIMPLOTRESNORM(X,OPTIMVALUES,STATE) plots OPTIMVALUES.resnorm.
%
%   Example:
%   Create an options structure that will use OPTIMPLOTRESNORM as the plot
%   function
%     options = optimoptions('lsqnonlin','PlotFcn',@optimplotresnorm);
%
%   Pass the options into an optimization problem to view the plot
%     lsqnonlin(@(x) sin(3*x),[1 4],[],[],options);

%   Copyright 2006-2015 The MathWorks, Inc.

persistent plotavailable
stop = false;

switch state
     case 'init'
         if isfield(optimValues,'resnorm')
             plotavailable = true;
         else
             plotavailable = false;
             title(getString(message('optim:optimplot:TitleNormResid', ...
                    getString(message('optim:optimplot:NotAvailable')))),'interp','none');
         end
    case 'iter'
        if plotavailable
            if optimValues.iteration == 0
                % The 'iter' case is  called during the zeroth iteration,
                % but it now has values that were empty during the 'init' case
                               
                %plot_topo = plot(optimValues.iteration,optimValues.resnorm,'kd', ...
                %    'MarkerFaceColor',[1 0 1]);
                    
                ppp = filter2pattern(x,elec);
                ppp_range = [-max(abs(ppp(:))) max(abs(ppp(:)))];
                plot_topo = plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', ppp_range, 'Shading', 'interp', 'Extrapolation', 1);                    
                    
                %xlabel(getString(message('optim:optimplot:XlabelIter')),'interp','none');
                %set(plot_topo,'Tag','plot_topo');
                %ylabel(getString(message('optim:optimplot:YlabelNormResid')),'interp','none');
                title('Src Topo','interp','none');
            else
                %plot_topo = findobj(get(gca,'Children'),'Tag','plot_topo');
                               
                ppp = filter2pattern(x,elec);
                ppp_range = [-max(abs(ppp(:))) max(abs(ppp(:)))];
                plot_topo = plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', ppp_range, 'Shading', 'interp', 'Extrapolation', 1);                    
                
%                 newX = [get(plot_topo,'Xdata') optimValues.iteration];
%                 newY = [get(plot_topo,'Ydata') optimValues.resnorm];
%                 set(plot_topo,'Xdata',newX, 'Ydata',newY);
%                 set(get(gca,'Title'),'String', ...
%                     getString(message('optim:optimplot:TitleNormResid', ...
%                     sprintf('%g',optimValues.resnorm))));

                title('Src Topo','interp','none');
            end
        end
end
