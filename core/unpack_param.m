function unpack_param (var, str)
% Given a structure with fields, spawns variables with the same names in the 'caller' workspace 
% Ugly temporary solution before I think of a better one :)
% 
% INPUT: 
%     var - structure variable
%     str - string name for the variable in the caller workspace
%
% OUTPUT:
%     none (just silently spawns lots of variables)

fns = fieldnames(var);
for i = 1:length(fns)
    evalin('caller', sprintf('%s = %s.%s;', fns{i}, str, fns{i}));
end

end