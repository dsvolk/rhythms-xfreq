function [e1, b1] = project_to_ortho ( e0, f1 )
% Calculates orthogonal projections of vectors to the subspace orthogonal to their linear combination.
% (Assumes that the vector system has full rank n, i.e, they constitute to an n-dimensional basis)
%
% In the new vector system of rank n-1, drops one vector to get a basis of n-1 vectors.
%
% Also calculates the coordinate change from the new basis to the old basis.
%
% INPUT: 
%     e0 - n row vectors of length N, matrix Nxn
%     f1 - linear combination of e0, as a weight vector nx1
%
% OUTPUT:
%     e1 - new matrix Nx(n-1) = e0 with e0*f1 projected out
%     b1 - coordinate transformation from the new vectors to old vectors, matrix nx(n-1)

assert (size(e0,2) == size (f1,1));
n = size(e0,2);

b0 = eye(n,n);
u1 = e0 * f1;

G = e0' * e0;

f1sq = f1' * G * f1;

%%
for i=1:n
    c(i) = b0(:,i)' * G * f1;
    e1(:,i) = e0(:,i) - (c(i) / f1sq) * u1;
    b1(:,i) = b0(:,i) - (c(i) / f1sq) * f1;
end

%%
% index to drop
[~,k] = max(abs(c));

e1(:,k) = [];
b1(:,k) = [];

%% test
% f2 = rand(n-1,1)-0.5;
% u2 = e1 * f2;
% 
% f2o = b1 * f2;
% u20 = e0 * f2o;
% 
% max(abs(u2-u20));

end