load Mnt2D;

fig = figure(1); clf
fig.Color = 'white';
fig.Name = sprintf('plot_scalp test');


ppp = ones(93,1);

%cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));
%logical_cells = cellfun(cellfind('Cz'), Mnt2D.clab);

%cellfind = @(string)(@(cell_contents)(ismember(cell_contents,string)));
%logical_cells = cellfun(cellfind({'Cz'}), Mnt2D.clab);

bad_cells = cell_idx(Mnt2D.clab, {'EOGLR', 'EOGLL', 'EOGC', '127', '128', 'EMG'});
numcells = sum(bad_cells);


ppp(bad_cells) = 2;

rangescalp = [ min(ppp) max(ppp)];
plot_scalp(Mnt2D, ppp, 'ShowLabels', 1, 'CLim', rangescalp, 'Shading', 'interp', 'Extrapolation', 1);
%plot_scalp(Mnt2D, ppp, 'CLim', rangescalp);
title(sprintf('plot_scalp test'));