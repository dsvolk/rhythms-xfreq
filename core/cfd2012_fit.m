function [filter] = cfd2012_fit ( elec_q, elec_warped )
% Linear fit from Nikulin, Nolte, Curio "Cross-frequency decomposition: A novel technique for studying interactions between neuronal oscillations with different frequencies", Clinical Neurophysiology, 2012
% 
% INPUT: 
%     elec_q - signals at frequency FrQ
%     elec_warped - signals at frequency Fr, Fr:FrQ = 1:q, freq warped up q times
%
% OUTPUT:
%     filter      - spatial filter (coefficients for linear combination of elec_q)


%% linear fit from the orginal CFD paper (Nikulin et al, 2012)
elec_q_cov = cov(elec_q);
rr = detrend(elec_warped,'constant');
filter = elec_q_cov \ elec_q' * rr;

end