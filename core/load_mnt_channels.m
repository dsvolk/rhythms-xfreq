function [GoodMnt2D, GoodChannels] = load_mnt_channels ()

load Mnt2D;
load ChannelTags;

GoodChannels = cell_idx (Mnt2D.clab', ChannelTags);
%GoodChannels = cell_idx (Mnt2D.clab', {'Cz', 'Fz', 'T7'});
default.SensorsNum = sum(GoodChannels);

GoodMnt2D = struct;
GoodMnt2D.x = Mnt2D.x(GoodChannels);
GoodMnt2D.y = Mnt2D.y(GoodChannels);
GoodMnt2D.pos_3d = Mnt2D.pos_3d(:,GoodChannels);
GoodMnt2D.clab = Mnt2D.clab(:,GoodChannels);

end