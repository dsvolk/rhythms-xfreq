function [ mat ] = right_stoch_matrix ( dim1, dim2 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

mat = rand(dim1, dim2);
rowsum = sum(mat,1);
mat = bsxfun(@rdivide, mat, rowsum);

end

