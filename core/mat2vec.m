function [V] = mat2vec (M)
%% Matrix to Vector

V = M(:);
end