function [src_sig_p, src_sig_q] = snr_adjust (PairsNum, snr, src_sig_p, src_sig_q, pattern_sig, var_elec_noise_p, var_elec_noise_q, compute_snr, normalize_snr)

SourceNum = 2*PairsNum;
T = size(src_sig_p,1);

%% Compute Signal-to-Noise Ratios for the ChNRef 'meaningful' signals compared to ChNSrc-ChNRef 'noise' signals
if compute_snr || normalize_snr
    snr_cur = NaN(1,SourceNum);
    
    %% FrP signals
    for k = 1:PairsNum
        source = zeros(T,SourceNum);
        source(:,k) = src_sig_p(:,k);
        elec_sig_p = source * pattern_sig';
        var_elec_sig_p = var(elec_sig_p);
        
        snr_cur(k) = sum(var_elec_sig_p) / var_elec_noise_p;
    end
    
    %% FrQ signals
    for k = 1:PairsNum
        source = zeros(T,SourceNum);
        source(:,PairsNum+k) = src_sig_q(:,k);
        elec_sig_q = source * pattern_sig';
        var_elec_sig_q = var(elec_sig_q);
        
        snr_cur(PairsNum+k) = sum(var_elec_sig_q) / var_elec_noise_q;
    end
    
    batchout(sprintf('Initial individual SNR for each of %d ''meaningful'' signals:', SourceNum));
    disp(snr_cur);
end
%% Normalize SNRs to a given value
if normalize_snr
    src_sig = [src_sig_p src_sig_q];
    
    snr_factor = sqrt(snr_cur / snr);
    src_sig = bsxfun(@rdivide,src_sig,snr_factor);
    
    src_sig_p = src_sig(:,1:PairsNum);
    src_sig_q = src_sig(:,PairsNum+1:SourceNum);
    
    %src_sig = src_sig ./ repmat(snr_factor,T,1);
else
    batchout('SNR correction is OFF.');
end
%% Double-check SNRs are OK now: Compute Signal-to-Noise Ratios for the ChNRef 'meaningful' signals compared to ChNSrc-ChNRef 'noise' signals
if normalize_snr && compute_snr
    snr_corrected = NaN(1,SourceNum);
    
    %% FrP signals
    for k = 1:PairsNum
        source = zeros(T,SourceNum);
        source(:,k) = src_sig_p(:,k);
        elec_sig_p = source * pattern_sig';
        var_elec_sig_p = var(elec_sig_p);
        
        snr_corrected(k) = sum(var_elec_sig_p) / var_elec_noise_p;
    end
    
    %% FrQ signals
    for k = 1:PairsNum
        source = zeros(T,SourceNum);
        source(:,PairsNum+k) = src_sig_q(:,k);
        elec_sig_q = source * pattern_sig';
        var_elec_sig_q = var(elec_sig_q);
        
        snr_corrected(PairsNum+k) = sum(var_elec_sig_q) / var_elec_noise_q;
    end
    
    batchout(sprintf('Corrected individual SNR for each of %d ''meaningful'' signals:', SourceNum));
    disp(snr_corrected);
end


end