function [filter_q] = gcfd_optimize ( m, r, FrP, f0_default, randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, ...
    sasha_optimization_tweak_on, sasha_f0_norm, sasha_step )
% Core function for Generalized Cross-Frequency Decomposition
%
% INPUT:
%     m - signals at frequency FrQ, complexified, matrix TxM
%     r - signal at frequency FrP, FrP:FrQ = p:q, complexified, freq warped up q times, matrix Tx1
%         r is supposed to be one "reference" component, known a-priori from other means
%     FrP - FrP, integer number
%
%     f0_default, randomize_filter_seeds, filter_seeds_num - settings for seeds for iterative nonlinear optimization
%     lsqnonlin_param - parameters for "lsqnonlin" function (MATLAB built-in)
% OUTPUT:
%     filter_q - spatial filters (demixing coefficients) for frequency FrQ, matrix MxN

assert(~sasha_optimization_tweak_on);

SensorsNum = size(m,2);
PairsNum = size(r,2);

assert(PairsNum == 1);

%% nonlinear least-squares optimization
%% define the function for 'lsqnonlin'
fun1 = @(f) abs((m*f).^FrP - r(:)); % fit r
fun2 = @(f) abs((m*f).^FrP + r(:)); % fit minus r

%% assemle the options structure for the algorithm
oo_names = fieldnames(lsqnonlin_param);
oo_values = struct2cell(lsqnonlin_param);
ooo(1:2:2*length(oo_names)-1) = oo_names;
ooo(2:2:2*length(oo_names)) = oo_values;
options = optimoptions('lsqnonlin', ooo{:});

%% set up the seeds and call 'lsqnonlin'

if randomize_filter_seeds
    %f0 reproducibility
    rng(1);

    batchout(sprintf('[Seed Mode] randomize_filter_seeds with %d seeds:', filter_seeds_num));
    
    filter_tmp = NaN(SensorsNum, 2*filter_seeds_num);
    resnorm = NaN(2*filter_seeds_num,1);
    
    for seed_idx = 1:filter_seeds_num
        batchout(sprintf('Random seeds %d/%d:', seed_idx, filter_seeds_num));
        
        f0 = randn(SensorsNum,1);
        f0 = f0/sum(abs(f0(:)));
        
        disp('Run for (m*f).^FrP - r(:) ...');
        [filter_tmp(:,seed_idx),resnorm(seed_idx),~,~,~] = lsqnonlin(fun1,f0,[],[],options);
        
        disp('Run for (m*f).^FrP + r(:) ...');
        [filter_tmp(:,filter_seeds_num+seed_idx),resnorm(filter_seeds_num+seed_idx),~,~,~] = lsqnonlin(fun2,f0,[],[],options);
    end
    
    [M,I] = min(resnorm(:));
    
    if filter_seeds_num > 1
        stdout(sprintf('Residual norms for %d seed filters:', filter_seeds_num));
        disp(resnorm(:));
        stdout(sprintf('Resnorms std/mean = %.4f\n', std(resnorm(:)) / mean(resnorm(:))));
    end
    
    filter_q = filter_tmp(:,I(1));
    
else
    batchout(sprintf('[Seed Mode] default.\n'));
    f0 = f0_default;
    filter_q = lsqnonlin(fun,f0,[],[],options);
end
    

end
