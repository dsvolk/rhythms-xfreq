function [pattern_sig, pattern_noise] = mk_model_mni (source_analysis, SourceNum, NoiseNum)

%% generate random dipole orientation vectors for synced sources and noise
dipole_orient_3d = randn(NoiseNum+SourceNum,3);    % Here we use 'randn' instead of 'rand' because 3 x gaussian is a 3d uniform gaussian
dipole_orient_3d = bsxfun(@rdivide,dipole_orient_3d,sqrt(sum(dipole_orient_3d.^2,2)));  % normalize to unit vectors

dipole_orient_3d_sig = dipole_orient_3d(1:SourceNum,:);
dipole_orient_3d_noise = dipole_orient_3d(SourceNum+1:SourceNum+NoiseNum,:);

%% generate random dipole positions for synced sources and noise
dipole_idx = randperm(size(source_analysis.cortex.vc,1));

dipole_pos_3d_sig = source_analysis.cortex.vc(dipole_idx(1:SourceNum),:);
dipole_pos_3d_noise = source_analysis.cortex.vc(dipole_idx(SourceNum+1:SourceNum+NoiseNum),:);

%% use Guido_toolbox to generate mixing patterns
pattern_sig = forward_general([dipole_pos_3d_sig dipole_orient_3d_sig],source_analysis.fp);
pattern_noise = forward_general([dipole_pos_3d_noise dipole_orient_3d_noise],source_analysis.fp);

end
