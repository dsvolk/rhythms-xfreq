function po = pattern_overlap (pat)

SIZE = size(pat,2);
po = NaN (SIZE);
for i=1:SIZE
    for j=1:SIZE
        if i ~= j
            po(i,j) = vec_mismatch (pat(:,i), pat(:,j));
        end
    end
end

end