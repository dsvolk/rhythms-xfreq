%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% building blocks for "playground"
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% test sync between Signal and Signal^2 (and between filtered(Signal^2))

randn('state',sum(100*clock))
Na1=randn(num_samp,sn); Na1=filtfilt(b_10,a_10,Na1);
for n=1:size(Na1,2)
    Na1(:,n)=Na1(:,n)/std(Na1(:,n));
end


%%
aaa = Na1(:,1);
bbb = aaa.^2;
bbb=filtfilt(b_20,a_20,bbb);

aaaang = angle(hilbert(aaa));
bbbang = angle(hilbert(bbb));

dat = [mod(aaaang, 2*pi), mod(bbbang, 2*pi)];
PhaseRel = mod(aaaang - bbbang, 2*pi);

hist3(dat,'Edges', {[0:pi/8:2*pi], [0:pi/8:2*pi]});
decorate_phase_phase;
xlabel('angle(aaa)');
ylabel('angle(aaa^{2})');
legend(sprintf('Kuramoto = %f', abs(mean(exp(1i.*PhaseRel(:))))));
hold off;
%%

%% nonlinear least-squares optimization plus phase parameter
%
% % set the initial values for coefficients 'p'
% p0 = ones(ChNAlpha,1)/ChNAlpha;
% theta0 = 0;
%
% x0 = [theta0' p0']';
%
% % define the function for 'lsqnonlin'
% fun = @(x) abs((m*x(2:end)).^FrR - exp(1i*x(1))*r); % default
% %fun = @(x) abs((m*x(2:end)).^FrR - r); % default
%
% % set up the noblinear least squares procedure
% options = optimoptions('lsqnonlin','Algorithm','levenberg-marquardt','Display','iter-detailed','TolFun',1e-6);
% [x,resnorm,residual,exitflag,output] = lsqnonlin(fun,x0,[],[],options);
%
% theta = x(1);
% p = x(2:end);

%% INVESTIGATE algo failures for FrSrc

figure; hold on;
for frr = 1:6
    sig = real(A.*exp(1i.*(frr*angle(z) + DelayBeta)));
    
    
    [P,f] = pwelch(sig,hanning(200),0,1024,200);
    semilogy(f,P);
    
    %plot(TShow, sig);
    lege{frr} = sprintf('frr = %d', frr);
end
legend(lege); grid on;
%decorate_signal;


ax = gca;
ax.ColorOrderIndex = 1;

for frr = 1:6
    [butter_test_b,butter_test_a]=butter(2,[frr*10-1 frr*10+1]/(Discr/2)); % References filter
    sig = filtfilt(butter_test_b,butter_test_a,rand(T,1));
    
    [P,f] = pwelch(sig,hanning(200),0,1024,200);
    semilogy(f,P,'LineWidth',2);
end

ax = gca;
ax.ColorOrderIndex = 1;
for frr = 1:6
    [butter_test_b,butter_test_a]=butter(2,[frr*10-1 frr*10+1]/(Discr/2)); % References filter
    sig = filtfilt(butter_test_b,butter_test_a,real(A.*exp(1i.*(frr*angle(z) + DelayBeta))));
    
    [P,f] = pwelch(sig,hanning(200),0,1024,200);
    semilogy(f,P,'--');
    %lege{frr} = sprintf('frr = %d', frr);
end
%legend(lege); grid on;
hold off;

%% INVESTIGATE PART 2

for frr = 1:6
    filename = sprintf('[frr=%d]',frr);
    figure('name',filename,'NumberTitle','off');
    hold on;
    
    %[butter_test_b,butter_test_a]=butter(2,[frr*10-1 frr*10+1]/(Discr/2)); % References filter
    %sig = filtfilt(butter_test_b,butter_test_a,real(A.*exp(1i.*(frr*angle(z) + DelayBeta))));
    sig = real(A.*exp(1i.*(frr*angle(z) + DelayBeta)));
    
    dat = [mod(angle(hilbert(sig)), 2*pi), mod(angle(z), 2*pi)];
    PhaseRel = mod(frr*angle(z) - angle(hilbert(sig)), 2*pi);
    
    hist3(dat,'Edges', {[0:pi/8:2*pi], [0:pi/8:2*pi]});
    decorate_phase_phase;
    xlabel('angle(warped\_and\_filtered)');
    ylabel('angle(z)');
    legend(sprintf('Kuramoto = %f', abs(mean(exp(1i.*PhaseRel(TShow))))));
    hold off;
end

%% END of INVESTIGATE

%% Some figures for filter recovery measure
thresh = 0.90;

filename = sprintf('[PARAM=%d FrSrc=%d FrRef=%d] filter_recovery_measure hist',PIDX,FrSrc,FrRef);
figure('name',filename,'NumberTitle','off'); histogram(filter_recovery_measure, -1:0.05:1); grid on; legend(sprintf('#(PRM > %.2f) = %d (%d %%)', thresh, sum(filter_recovery_measure(:) > thresh), floor(sum(filter_recovery_measure(:) > thresh) / numel(filter_recovery_measure) * 100)));
savefig(filename);
%autoArrangeFigures(1,2);

%FontSmoothing set to 'off'

%%
filename = sprintf('[PARAM=%d FrSrc=%d FrRef=%d] filter_recovery_measure',PIDX,FrSrc,FrRef);
figure('name',filename,'NumberTitle','off');
hold on;
if ~plot_trials_as_lines
    plot(filter_recovery_measure(:),'*');
    xlabel('Trials, Refs');
else
    for TIDX = 1:batch_size
        line([TIDX TIDX],[min(filter_recovery_measure(TIDX,:)), max(filter_recovery_measure(TIDX,:))]);
    end
    
    for k = 1:ChNRef
        plot(1:batch_size,filter_recovery_measure(:,k),'b*','DisplayName',['Pair ' num2str(k)]);
    end
    
    xlabel('Trials');
end
grid on; ylabel('Filter recovery measure');
hold off;
savefig(filename);

%% load real EEG data (from Zafer Iscan, 90 sec resting state EEG, discr = 1000)
clear cnt;
load('rest_data_Zafer_2016_07_08', 'cnt');
chan = find(strcmp(cnt.clab,'Oz'));    % look for OZ channel

Signal = cnt.x(:,chan);
Discr = cnt.fs;
T = size(Signal);
Time = 1./Discr*[1:T];

TShow = [1:T];
TShowDiff = [TShow(1):TShow(end)-1];

TargetAlpha = Signal;

clear cnt chan Signal

%% phase differences histogram
phase_diff = (mod(UAngle(TShow) - FrR*TargetAlphaAngle(TShow), 2*pi));
histogram(phase_diff(TShow), 0:pi/8:2*pi); decorate_phase_hist; legend(sprintf('\nTime window = [%d, %d]\nIn seconds = [%d,%d]\nKuramoto = %f', TShow(1), TShow(end), round((TShow(1)-1)/Discr), round(TShow(end)/Discr), abs(mean(exp((2*pi*i).*phase_diff(TShow))))));


%% detect possible beta and alpha phase slips (vertical lines)
plot(TShow, 2*unwrap(TargetAlphaAngle(TShow))/2/pi,'bs-', TShow, unwrap(UAngle(TShow))/2/pi,'ro-'); decorate_phase; legend('TargetAlphaAngle','UAngle');

%%
%%%%[hAx,hLine1,hLine2] = plotyy(Tshow, Vbeta(Tshow), Tshow, unwrap(Vbetaangle(Tshow))/2/pi); grid on; legend('Vbeta','Vbetaangle');
%%%%hLine1.Marker = 'o';
%%%%hLine2.Marker = '*';

SlipsAlphaMask = (pi/2 < DTargetAlphaAngle) & (DTargetAlphaAngle < 3*pi/2);
SlipsAlpha = find((pi/2 < DTargetAlphaAngle) & (DTargetAlphaAngle < 3*pi/2));
SlipsUAlphaMask = (pi/2 < DUAngle) & (DUAngle < 3*pi/2);
SlipsUAlpha = find((pi/2 < DUAngle) & (DUAngle < 3*pi/2));

Mask = SlipsUAlphaMask(TShowDiff) | SlipsAlphaMask(2*TShowDiff) | SlipsAlphaMask(2*TShowDiff+1);

% look for the longest slip-less interval
k = 1;
longest = 0;
longest_start = 0;
longest_finish = 0;
while k <= TShowDiff(end)
    if Mask(k)
        k = k+1;
    else
        start = k;
        while k <= TShowDiff(end) & ~Mask(k)
            k = k+1;
        end
        finish = k-1;
        if finish - start > longest
            longest = finish - start;
            longest_start = start;
            longest_finish = finish;
        end
    end
end

hold on;
for k = SlipsAlpha
    %for k = find(Vbetaampli < 0.1)
    plot([k k],ylim,'Color',[0.25 0.25 0.75]); % dark blue
end

%for k = find(PhaseDiffBeta < pi/8 | PhaseDiffBeta > 3*pi/8)
for k = SlipsUAlpha
    %for k = find(Valphaampli < 0.1)
    
    plot([k k],ylim,'Color',[0.75 0.25 0.25]); % dark red
end

k = [longest_start, longest_finish, 2*longest_start, 2*longest_finish]';
plot([k k],ylim,'Color',[0.25 0.75 0.25],'LineWidth',2); % dark green
hold off;

%% compute the phase derivatives
DPhaseAlpha(TShowDiff) = mod(diff(VAlphaAngle(TShow)), 2*pi);
DPhaseBeta(TShowDiff) = mod(diff(VBetaAngle(TShow)), 2*pi);

%% detect possible beta and alpha phase slips (vertical lines)
plot(TShow, unwrap(VAlphaAngle(TShow))/2/pi,'bs-', TShow, unwrap(VBetaAngle(TShow))/2/pi,'ro-'); decorate_phase; legend('VAlphaAngle','VBetaAngle');

%%%%[hAx,hLine1,hLine2] = plotyy(Tshow, Vbeta(Tshow), Tshow, unwrap(Vbetaangle(Tshow))/2/pi); grid on; legend('Vbeta','Vbetaangle');
%%%%hLine1.Marker = 'o';
%%%%hLine2.Marker = '*';

SlipsAlpha = find((pi/2 < PhaseDiffAlpha) & (PhaseDiffAlpha < 3*pi/2));
SlipsBeta = find((pi/2 < PhaseDiffBeta) & (PhaseDiffBeta < 3*pi/2));

hold on;
for k = SlipsAlpha
    %for k = find(Vbetaampli < 0.1)
    plot([k k],ylim,'Color',[0.25 0.25 0.75]); % dark blue
end

%for k = find(PhaseDiffBeta < pi/8 | PhaseDiffBeta > 3*pi/8)
for k = SlipsBeta
    %for k = find(Valphaampli < 0.1)
    
    plot([k k],ylim,'Color',[0.75 0.25 0.25]); % dark red
end
hold off;

%% create surrogate alpha and beta using plain sine
sinalpha = sin(2*pi*10*(Time'));
sinbeta = sin(2*pi*20*(Time'));
%sin(2*pi*9*(Time')) +

SourceAlpha(:,1:ChNAlpha) = sinalpha;  % pure sine, freq = 10 Hz
SourceBeta(:,1:ChNBeta) = sinbeta;  % pure sine, freq = 20 Hz
TargetAlpha = sinalpha;  % pure sine, freq = 10 Hz

clear sinalpha sinbeta

%% check the spectrum of alpha/beta

% [P,f]=pwelch(VBeta,hanning(200),0,1024,200);
% semilogy(f,P); grid on;
%
% clear f P

%% Prepare for gradient descent optimization

[MultiIndex,MultiCoef] = multinomial_expand(FrR,ChNAlpha);  % compute multinomial coefs

% x = [5,6];x
% sum(MultiCoef .* prod(repmat(x,size(MultiIndex,1),1).^MultiIndex,2));

Alpha = hilbert(VAlpha);
Beta = hilbert(VBeta);

%% compute the phase differences

PhaseDiffAlpha(TShowDiff) = mod(diff(VAlphaAngle(TShow)), 2*pi);
PhaseDiffBeta(TShowDiff) = mod(diff(VBetaAngle(TShow)), 2*pi);

PhaseRel(TShow) =  mod(FrR*VAlphaAngle(TShow,ChanAlpha) - VBetaAngle(TShow,ChanBeta), 2*pi);



%% 2D phase-phase histogram
% dat = [mod(Valphaangle(Tshow), 2*pi),mod(Vbetaslowedangle(Tshow), 2*pi)];
dat = [mod(VAlphaAngle(TShow,ChanAlpha), 2*pi), mod(VBetaAngle(TShow,ChanBeta), 2*pi)];
hist3(dat,'Edges', {[0:pi/8:2*pi], [0:pi/8:2*pi]});
decorate_phase_phase;
xlabel('VAlphaAngle');
ylabel('VBetaAngle');
legend(sprintf('\nChannel = %d\nTime window = [%d, %d]\nIn seconds = [%d,%d]\nKuramoto = %f\nSNR = %f', Chan, TShow(1), TShow(end), round((TShow(1)-1)/Discr), round(TShow(end)/Discr), abs(mean(exp(1i.*PhaseRel(TShow)))), SNRBeta));

%% phase differences histogram
histogram(phase_diff_beta(Tshowdiff), 0:pi/128:2*pi); decorate_phase_hist; legend(sprintf('Phase diff beta\nTime window = [%d, %d]\nIn seconds = [%d,%d]', Tshow(1), Tshow(end), round((Tshow(1)-1)/Tdiscr), round(Tshow(end)/Tdiscr)));
histogram(phase_diff_alpha(Tshowdiff), 0:pi/128:2*pi); decorate_phase_hist; legend(sprintf('Phase diff alpha\nTime window = [%d, %d]\nIn seconds = [%d,%d]', Tshow(1), Tshow(end), round((Tshow(1)-1)/Tdiscr), round(Tshow(end)/Tdiscr)));
