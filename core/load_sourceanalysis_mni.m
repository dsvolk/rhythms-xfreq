function [source_analysis, GoodChannels] = load_sourceanalysis_mni ()
%% Deprecated. Do not use! 

assert(false);

load clab_64;
source_analysis = prepare_sourceanalysis(clab_64,'montreal');
electrodes_num = size(source_analysis.clab_electrodes,2);

if electrodes_num ~= 64
    batcherr('File "clab_64" is probably corrupt. Wrong number of electrodes.');
    return;
end

GoodChannels = [1:64];

end
