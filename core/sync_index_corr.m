function [k] = sync_index_corr ( s1, s2, f1, f2, maxlag )
% Calculates "Correlation-based Sync Index", a proxy for measure for phase-phase synchronization of two signals
% (possibly of different but rationally related frequencies)
% 
% INPUT: 
%     s1,s2 - narrow-band signals of same length, Tx1
%     f1,f2 - integers proportional to their frequencies
%     maxlag - limits the lag range from -maxlag to maxlag
%
% OUTPUT:
%     k - Sync index

c1 = hilbert(s1);
c2 = hilbert(s2);

s1_warped = real(abs(c1).*exp(1i.*(f2*angle(c1))));
s2_warped = real(abs(c2).*exp(1i.*(f1*angle(c2))));

[acor,lag] = xcorr(s1_warped, s2_warped, floor(maxlag), 'coeff');
%[~,I] = max(abs(acor));
k = max(abs(acor));

end