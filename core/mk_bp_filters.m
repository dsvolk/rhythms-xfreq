function [butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a] = ...
    mk_bp_filters ( FrP, FrQ, FrBase, Discr )

[butter_base_b,butter_base_a]=butter(2,[9 11]/(Discr/2));           % Synchronized pairs band-pass filter (to be freq up-warped later)
[butter_p_b,butter_p_a]=butter(2,[FrP*FrBase-1 FrP*FrBase+1]/(Discr/2));    % FrP band-pass filter
[butter_q_b,butter_q_a]=butter(2,[FrQ*FrBase-1 FrQ*FrBase+1]/(Discr/2));    % FrQ band-pass filter
[butter_pq_b,butter_pq_a]=butter(2,[FrP*FrQ*FrBase-3 FrP*FrQ*FrBase+3]/(Discr/2));    % FrP*FrQ band-pass filter

end