function [ vr_idx, v_idx, mismatch ] = match_patterns_Volk2 ( vr, v, K )
% Produces the list of "K" best matches of columns of "v" to columns of "vr" 
% 
% INPUT: 
%     vr - matrix of "reference" vectors
%     v  - matrix of vectors to be compared against the references
%          1st dimension of vr and v must agree.
%     K  - number of best matches, K <= size(v,2)
%
% OUTPUT:
%     vr_idx      - indices in vr
%     v_idx       - indices in v
%     mismatch(n) - corresponding vec_mismatch measure for vr(vr_idx(n)) and v(v_idx(n))

N1 = size(vr,2);
N2 = size(v,2);

if size(v,1) ~= size(vr,1)
    error('Wrong argument, should be size(v,1) == size(vr,1).');
end

if K > N2
    error('Wrong argument, should be K <= size(v,2).');
end

mismatch_matrix = NaN (N1, N2);
for i = 1:N1
    for j = 1:N2
        mismatch_matrix(i,j) = vec_mismatch (vr(:,i), v(:,j));
    end
end

mismatch = NaN (K,1);
vr_idx = NaN (K,1);
v_idx = NaN (K,1);
for k = 1:K
    [mismatch(k), idx] = min(mismatch_matrix(:));
    [vr_idx(k), v_idx(k)] = ind2sub(size(mismatch_matrix),idx);

    % exclude the found row and column from further search - by setting them way above possible maximum
    mismatch_matrix(vr_idx(k),:) = Inf;
    mismatch_matrix(:,v_idx(k)) = Inf;
end

end