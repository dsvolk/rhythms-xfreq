function [i] = invperm ( p )

i = 1:length(p);
i(p) = i;

end
