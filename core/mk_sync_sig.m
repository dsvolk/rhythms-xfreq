function [src_sig_p, src_sig_q] = mk_sync_sig (T, PairsNum, butter_base_b, butter_base_a, FrP, FrQ, ampli_flip, normalize_sources, DelayAlpha, DelayBeta)

%% create PairsNum proto-signals using filtered white noise
target = filtfilt(butter_base_b,butter_base_a,randn(T,PairsNum));
target = bsxfun(@rdivide,target,std(target,0,1));

%% freq warp to generate ChNRef "target" beta sources in sync with "target alpha"
target_cplx = hilbert(target);

target_abs_p = abs(target_cplx);
if ampli_flip
    target_abs_q = flip(target_abs_p);
else
    target_abs_q = target_abs_p;
end

src_sig_p = real(target_abs_p.*exp(1i.*(FrP*angle(target_cplx) + DelayAlpha)));
src_sig_q = real(target_abs_q.*exp(1i.*(FrQ*angle(target_cplx) + DelayBeta)));

%% normalize all signals
if normalize_sources
    batcherr('normalize_sources is not currently supported!');
    % Denis: I am not yet sure how to properly normalize them, so let's just skip for now.
    return;

    for k = 1:PairsNum
        src_norm = mean(abs(hilbert(src_sig_p(k))));
        src_sig_p(k) = src_sig_p(k) / src_norm;
    end
    
    for k = 1:PairsNum
        src_norm = mean(abs(hilbert(src_sig_q(k))));
        src_sig_q(k) = src_sig_q(k) / src_norm;
    end
end

end