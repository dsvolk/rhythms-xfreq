%% [script control] experiment parameters

% default parameters for experiments
%default.Discr = 500;        % time discretization, Hz
%default.Secs = 300;         % overall signal time, sec
%default.Secs = 302;

rng_seed = rng_seed_default;    % new every run, based on the current time
%rng_seed = rng_saved_states(1);        % choose from manually pre-loaded seeds

default.FwdModel = FwdModelType.Data;  % choose one of the above options
default.DataFileName = '20170410_HatA_1';
default.LayoutFileName = '20170410_Layout';
default.SkipChannels = {'AF3'};     % ignore these channels
default.epochs_default = [];        % if epochs_load_on == false, this epochs will be used. Leave empty to use all time range.
default.epochs_load_on = true;     % load the 'epochs' information about bad data sectors. Affects SSD, gCFD, sync indices. Doesn't affect spectrum plots, CFD'2012 ! 

default.seed_pat_sig   = randi([0 9999999]);
default.seed_pat_noise = randi([0 9999999]);
default.seed_src_sig   = randi([0 9999999]);
default.seed_src_noise = randi([0 9999999]);

default.fr_cutoff_low = 0.5;        % for preprocessing and spectrum display
default.fr_cutoff_high = 40.0;

default.PairsNum = 5;       % number of synced source pairs
default.NoiseNum = 100;       % number of (pink) noise sources

%%deprecated. Now the number of sensors is derived from ElecList when necessary
%default.SensorsNum = NaN;     % number of sensors (electrodes etc.)

default.FrBase = 9;    % base frequency, Hz - to be multiplied by FrP, FrQ
default.FrP = 2;        % frequency ratio - (where the nonlinearity hides)
default.FrQ = 1;        % frequency ratio - (harmless)

default.batch_size = 1;   % number of test for each setting

default.normalize_sources = false;
default.targets_abs_1 = false;

default.ampli_flip = false;

default.compute_snr = true;
default.normalize_snr = true;
default.SNR = 1.0;  % signal-to-noise ratio

default.ssd_primary_on = true;  % you can disable it in FWD_MODEL_RAND or FWD_MODEL_GUIDO_MNI64 modes
default.ssd_secondary_on = true;  % you can disable it in all modes
default.ssd_secondary_comp_num = 15;    % set to Inf to use all the ssd_q components

default.ssd_filter_offset_start = 200;
default.ssd_filter_offset_end = 200;
default.plot_ssd_lambda = false;
default.plot_ssd_topo = false;

% only one of these should be true at a time
default.randomize_filter_seeds = false;
default.filter_seeds_num = 5;
default.filter_seed_cfd2012 = false;
default.filter_seed_ssd = false; % only possible with "ssd_secondary_on = false"

default.post_warp_pq_filter_on = false;

default.project_out_found_sources = false;  % each found source is projected out of the source space, iteratively

default.lsqnonlin_param.Algorithm = 'levenberg-marquardt';
default.lsqnonlin_param.Display = 'final-detailed';
default.lsqnonlin_param.TolFun = 1e-8;
default.lsqnonlin_param.MaxFunEvals = 1000000;
default.lsqnonlin_param.MaxIter = 100;
%default.lsqnonlin_param.PlotFcn = {@optimplotx, @optimplotfunccount, @optimplotfval, @optimplotresnorm, @optimplotstepsize, @optimplotfirstorderopt};
default.lsqnonlin_param.PlotFcn = [];
%default.lsqnonlin_param.UseParallel = false;

%default.lsqnonlin_param.ScaleProblem = 'jacobian';  % option for 'levenberg-marquardt'
default.lsqnonlin_param.ScaleProblem = 'none';  % option for 'levenberg-marquardt'

default.norm_matrix_ord_on = false;
default.sasha_optimization_tweak_on = false;
default.sasha_f0_norm = 1;
default.sasha_step = 3;

default.lsqnonlin_inject_topo_plot = false;

default.pre_debug_thresh = 2;    % Possible values: 0..1. Set 2 to disable threshold

default.xtest_pq = false;

default.plot_pre_hist = false;
default.plot_pre_trials = false;
default.plot_trials_as_lines = true;
default.plot_ssd_error_hist = true;
default.plot_dual_pre_ssd_error_hist = true;
plot_pre_as_heatmap = true;

plot_snr_vs_pre_median = true;

default.plot_separate_hists_for_ssd_comps = false;   % warning: PairsNum* more figures!

default.source_topo_figures = false;
default.source_topo_figures_normalize_ranges_across_scalps = false;

default.calc_sync_measures = true;

default.experimenal_freq_warp_offset = 0;

plotting_on = true;
sound_on_finish = true;

%% PARAMS 
PARAMS = 1:1;
%PARAMS = 1:1;
param(PARAMS) = default;

% For the composite PRE figure
[Comp_PRE_Fig_Cols, Comp_PRE_Fig_Rows] = figuresGrid(length(PARAMS));

%% set non-default parameters

% [6] 'R.6' First batch tests with a realistic forward model
% FrP = [2 1 2 2 3 3];
% FrQ = [1 3 1 3 1 2];

% [1] 'R.1' Benchmark for comparison w m_n_synchronization_simulation script by Vadim Nikulin
% FrP = [2];
% FrQ = [3];

%experimenal_freq_warp_offset = [-0.5*pi -0.25*pi 0 0.25*pi 0.5*pi];
%experimenal_freq_warp_offset = [0 0.125*pi 0.25*pi 0.375*pi 0.5*pi];
%experimenal_freq_warp_offset = [-0.5*pi -0.375*pi -0.25*pi -0.125*pi 0 0.125*pi 0.25*pi 0.375*pi 0.5*pi];

for j = PARAMS
    %param(j).FrP = FrP(j);
    %param(j).FrQ = FrQ(j);
    
    %param(j).experimenal_freq_warp_offset = experimenal_freq_warp_offset(j);
    
    %param(j).T = param(j).Discr * param(j).Secs;     % number of time ticks    
    param(j).SourceNum = param(j).PairsNum * 2;
    
    param(j).cur_param_idx = j;
end
