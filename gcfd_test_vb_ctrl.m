%% [script control] experiment parameters

% default parameters for experiments
default.Discr = 200;        % time discretization, Hz
%default.Secs = 32;         % overall signal time, sec
default.Secs = 152;
%default.Secs = 302;

rng_seed = rng_seed_default;    % new every run, based on the current time
%rng_seed = rng_saved_states(1);        % choose from manually pre-loaded seeds

default.FwdModel = FwdModelType.GuidoMNI;  % choose one of the above options
default.ElecList = clab_10;
default.fr_cutoff_low = 0.5;      % for preprocessing and spectrum display
default.fr_cutoff_high = 40.0;

%default.seed_src_sig   = 5032251;
%default.seed_src_noise = 2931344;
%default.seed_pat_all = 3304517;

default.PairsNum = 5;       % number of synced source pairs
default.NoiseNum = 100;       % number of (pink) noise sources

%%deprecated. Now the number of sensors is derived from ElecList when necessary
%default.SensorsNum = NaN;     % number of sensors (electrodes etc.)

default.FrBase = 10;    % base frequency, Hz - to be multiplied by FrP, FrQ
default.FrP = 2;        % frequency ratio - (where the nonlinearity hides)
default.FrQ = 3;        % frequency ratio - (harmless)

default.batch_size = 10;   % number of tests for each setting

default.normalize_sources = false;
default.targets_abs_1 = false;

default.ampli_flip = false;

default.compute_snr = true;
default.normalize_snr = true;
default.SNR = 1.0;  % signal-to-noise ratio

default.ssd_primary_on = true;  % if disabled, then the true sources are used as references for gCFD
default.ssd_secondary_on = false;
default.ssd_secondary_comp_num = 15;    % set to Inf to use all the ssd_q components

default.ssd_filter_offset_start = 200;
default.ssd_filter_offset_end = 200;
default.plot_ssd_lambda = false;

% only one of these should be true at a time
default.randomize_filter_seeds = true;
default.filter_seeds_num = 5;
default.filter_seed_cfd2012 = false;
default.filter_seed_ssd = false; % only possible with "ssd_secondary_on = false"

default.post_warp_pq_filter_on = false;

default.project_out_found_sources = false;  % each found source is projected out of the source space, iteratively

default.lsqnonlin_param.Algorithm = 'levenberg-marquardt';
default.lsqnonlin_param.Display = 'final-detailed';
default.lsqnonlin_param.TolFun = 1e-8;
default.lsqnonlin_param.MaxFunEvals = 1000000;
default.lsqnonlin_param.MaxIter = 400;
%default.lsqnonlin_param.PlotFcn = {@optimplotx, @optimplotfunccount, @optimplotfval, @optimplotresnorm, @optimplotstepsize, @optimplotfirstorderopt};
default.lsqnonlin_param.PlotFcn = [];
%default.lsqnonlin_param.UseParallel = false;
%default.lsqnonlin_param.ScaleProblem = 'jacobian';  % option for 'levenberg-marquardt'
default.lsqnonlin_param.ScaleProblem = 'none';  % option for 'levenberg-marquardt'

default.norm_matrix_ord_on = false;
default.sasha_optimization_tweak_on = false;
default.sasha_f0_norm = 1;
default.sasha_step = 3;

default.lsqnonlin_inject_topo_plot = false;

default.pre_debug_thresh = 2;    % Possible values: 0..1. Set 2 to disable threshold

default.xtest_pq = false;

default.plot_pre_hist = false;
default.plot_pre_trials = false;
default.plot_trials_as_lines = true;

default.plot_ssd_error_hist = false;
default.plot_ssd_error_vs_pre = false;

plot_pre_as_heatmap = true;
plot_pre_median_as_heatmap = true;

default.plot_separate_hists_for_ssd_comps = false;   % warning: PairsNum* more figures!

default.source_topo_figures = false;
default.source_topo_figures_normalize_ranges_across_scalps = false;

default.calc_sync_measures = true;

plotting_on = true;
sound_on_finish = true;

%% PARAMS 
%% set non-default parameters
% 'R.1' Benchmark for comparison w m_n_synchronization_simulation script by Vadim Nikulin
% FrP = [2];
% FrQ = [1];

% 'F.4' Final four for the paper
% FrP = [1 2 2 3];
% FrQ = [2 1 3 2];

% 'R.8'
% FrP = [1 2 2 2 3 3 3 4];
% FrQ = [3 1 3 5 1 2 5 1];

% 'R.9'
% FrP = [1 1 2 2 2 3 3 3 4];
% FrQ = [2 3 1 3 5 1 2 5 1];

% 'R.20'
FrP = [1 1 1 1 2 2 2 2 3 3 3 3 4 4 4 4 5 5 5 5];
FrQ = [2 3 4 5 1 3 4 5 1 2 4 5 1 2 3 5 1 2 3 4];

% Single run
% FrP = [2];
% FrQ = [3];

assert(2*default.FrBase*max(FrP)*max(FrQ) < default.Discr);

%% For a single SNR
PARAMS = 1:length(FrP);
param(PARAMS) = default;

for j = PARAMS
    param(j).FrP = FrP(j);
    param(j).FrQ = FrQ(j);
    
    param(j).T = param(j).Discr * param(j).Secs;     % number of time ticks    
    param(j).SourceNum = param(j).PairsNum * 2;
end

%% For multiple SNRs 
% SNRs = [1.0 0.5 0.1 0.05];
% 
% PARAMS = 1:length(FrP)*length(SNRs);
% param(PARAMS) = default;
% 
% j = 1;
% for k = 1:length(SNRs)
%     for i = 1:length(FrP)
%         
%         param(j).FrP = FrP(i);
%         param(j).FrQ = FrQ(i);
%         param(j).SNR = SNRs(k);
%         
%         param(j).T = param(j).Discr * param(j).Secs;     % number of time ticks
%         param(j).SourceNum = param(j).PairsNum * 2;
%         
%         j=j+1;
%     end
% end

%% For multiple Secs 
% Secs = [32 62 152 302];
% 
% PARAMS = 1:length(FrP)*length(Secs);
% param(PARAMS) = default;
% 
% j = 1;
% for k = 1:length(Secs)
%     for i = 1:length(FrP)
%         
%         param(j).FrP = FrP(i);
%         param(j).FrQ = FrQ(i);
%         param(j).Secs = Secs(k);
%         
%         param(j).T = param(j).Discr * param(j).Secs;     % number of time ticks
%         param(j).SourceNum = param(j).PairsNum * 2;
%         
%         j=j+1;
%     end
% end

%% For the composite PRE figure
Comp_PRE_Fig_Cols = 4;
Comp_PRE_Fig_Rows = 5;
%[Comp_PRE_Fig_Cols, Comp_PRE_Fig_Rows] = figuresGrid(length(PARAMS));
