function [h] = plot_fft (X, Discr) 
% Plot power spectrum of signal(s)
% 
% INPUT: 
%     X - signals, matrix TxN, T time ticks and N signals
%     Discr - time discretization, Hz
%
% OUTPUT:
%     h - handle to the plot

T = size(X,1);

%% Fourier Transform:
Y = fft(X);

%% Take one half of that
Y = Y(1:size(Y,1)/2,:);

%% Frequency specifications:
dF = Discr/T;                      % hertz
f_scale = 0:dF:Discr/2-dF;           % hertz

%% Plot the power spectrum:
%semilogy(f_scale,abs(Y).^2/N); grid on; grid minor;
h=plot(f_scale,abs(Y).^2/T); grid on; grid minor;
xlabel('Frequency (Hz)');
ylabel('Power');