grid on; 

h1 = zoom;
h2 = pan;
set(gca,'XTickLabelMode','auto');
set(gca, 'XTickLabel', num2str(get(gca,'XTick')','%d'))
set(h1,'ActionPostCallback',@mypostcallbackX);
set(h2,'ActionPostCallback',@mypostcallbackX);