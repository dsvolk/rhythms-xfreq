function figuresToPNGs()
%
% Version 1.0 by Denis Volk, 2016.08.24
% dire.ulf@gmail.com

figHandle = flip(findobj('Type','figure'));

for k = 1:size(figHandle)
    print(figHandle(k),figHandle(k).Name,'-dpng')
end

return;