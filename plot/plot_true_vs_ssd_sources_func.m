function [fig_idx] = plot_true_vs_ssd_sources_func ( fig_handles, fig_idx, freq, lambda, pattern_p, PairsNum, GoodMnt2D )

[fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );

fig.Color = 'white';
colormap('jet');
fig.Name = sprintf('Fig. %d: SSD %dHz sources', fig_idx, FrP*FrBase);
for n=1:PairsNum
    subplot(2,PairsNum,n);   % Top: true alpha sources
    title(sprintf('True %dHz #%d', FrP*FrBase, pat_sig_p_idx(n)));
    showfield(pattern_sig_p(:,pat_sig_p_idx(n)), source_analysis.locs_2D);
    
    subplot(2,PairsNum,n+PairsNum);  % Bottom: SSD recovered alpha sources
    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f\nError = %.4f', FrP*FrBase, pat_p_idx(n), '{\lambda}', lambda(pat_p_idx(n)),  PRE_1_ssd_primary(TIDX,n)));
    showfield(pattern_p(:,pat_p_idx(n)), source_analysis.locs_2D);
end

drawnow;

end