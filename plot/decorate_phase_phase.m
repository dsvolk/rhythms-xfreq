set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
set(gcf,'renderer','opengl');
alpha(0.8);
% change the transparency
% alpha(0.5);
% % define the color of the face according to the height (left and right)
% set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
% % define the color of the face according to the height (center)
% set(get(gca,'child'),'FaceColor','flat','CDataMode','auto');
% % change the colormap
% colormap('cool');
% % accelerate the display using OpenGL for the rendering
% set(gcf,'renderer','opengl');


colorbar;
grid on;
view(2);
ax = gca;
ax.XTick = [0 0.25*pi 0.5*pi 0.75*pi pi 1.25*pi 1.5*pi 1.75*pi 2*pi];
ax.XTickLabel = {'0','\pi/4','\pi/2','3\pi/4','\pi','5\pi/4','3\pi/2','7\pi/4','2\pi'};
xlim([0 2*pi]);
ax.YTick = [0 0.25*pi 0.5*pi 0.75*pi pi 1.25*pi 1.5*pi 1.75*pi 2*pi];
ax.YTickLabel = {'0','\pi/4','\pi/2','3\pi/4','\pi','5\pi/4','3\pi/2','7\pi/4','2\pi'};
ylim([0 2*pi]);