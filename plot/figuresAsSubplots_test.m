hf(1) = figure(1);
plot(peaks);

hf(2) = figure(2);
plot(membrane);

figuresAsSubplots();

return;

figHandle = flip(findobj('Type','figure'));

hf(3) = figure(3);
ha(1) = subplot(1,2,1);
ha(2) = subplot(1,2,2);

for i = 1:2
    hc  = get(hf(i),'children');
    hgc = get(hc, 'children');
    copyobj(hgc,ha(i));
end