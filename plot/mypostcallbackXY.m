function mypostcallbackXY(obj,evd)
set(gca,'XTickLabelMode','auto');
set(gca, 'XTickLabel', num2str(get(gca,'XTick')','%d'));
set(gca,'YTickLabelMode','auto');
set(gca, 'YTickLabel', num2str(get(gca,'YTick')','%d'));