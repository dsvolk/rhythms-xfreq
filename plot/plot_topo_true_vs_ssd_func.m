function [fig_idx] = plot_topo_true_vs_ssd_func ( fig_handles, fig_idx, str, output_dir, freq, lambda, PairsNum, ...
    extra_cpts_num, pattern_sig_p, pat_sig_p_idx, src_sig_p, pattern_p, pat_p_idx, X_ssd_p, PRE_1_ssd_primary, source_analysis )
% 
% INPUT: 
%     extra_cpts_num - total number of SSD components plotted will be min(length(lambda), PairsNum+extra_cpts_num)
%
% OUTPUT:
%     fig_idx - script plotting variable

[fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );

fig.Color = 'white';
colormap('jet');
fig.Name = sprintf('Fig. %d: SSD %dHz sources %s', fig_idx, freq, str);
filename = sprintf('[Fig %d] SSD %dHz sources %s', fig_idx, freq, str);

assert(extra_cpts_num >= 0);
ssd_comps_to_plot = min(length(lambda), PairsNum+extra_cpts_num);

% for n=1:PairsNum
%     subplot(2,PairsNum,n);   % Top: true alpha sources
%     title(sprintf('True %dHz #%d', freq, pat_sig_p_idx(n)));
%     showfield(pattern_sig_p(:,pat_sig_p_idx(n)), source_analysis.locs_2D);
%     
%     subplot(2,PairsNum,n+PairsNum);  % Bottom: SSD recovered alpha sources       
% %     if calc_sync_measures
% %         kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_ssd_idx(n)), sig, 1, 1 );  % sync measure between the true signal at FrQ and the de-mixed (by SSD q) component
% %         %kappa2 = kuramoto ( X_ssd_p(:,pat_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD p component at FrP and the de-mixed (by SSD q) component at FrQ
% %         kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the de-mixed (by SSD q) component at FrQ
% %         str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
% %     else
% %         str_sync_measures = '';
% %     end    
% %     title(sprintf('SSD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_ssd_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_ssd_idx(n)), pattern_q_ssd(:,pat_q_ssd_idx(n))), str_sync_measures));
%     
%     title(sprintf('SSD %dHz #%d\nSSD %s = %.4f\nError = %.4f', freq, pat_p_idx(n), '{\lambda}', lambda(pat_p_idx(n)),  PRE_1_ssd_primary(n)));
%     showfield(pattern_p(:,pat_p_idx(n)), source_analysis.locs_2D);   
% end

for n=1:PairsNum
    subplot(2,ssd_comps_to_plot,n);   % Top: true alpha sources
    title(sprintf('True %dHz #%d', freq, pat_sig_p_idx(n)));
    showfield(pattern_sig_p(:,pat_sig_p_idx(n)), source_analysis.locs_2D);
end
    
for n=1:PairsNum
    subplot(2,ssd_comps_to_plot,n+ssd_comps_to_plot);  % Bottom: SSD recovered alpha sources           
    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f\nError = %.4f', freq, pat_p_idx(n), '{\lambda}', lambda(pat_p_idx(n)),  PRE_1_ssd_primary(n)));
    showfield(pattern_p(:,pat_p_idx(n)), source_analysis.locs_2D);   
end

for n=PairsNum+1:ssd_comps_to_plot
    subplot(2,ssd_comps_to_plot,n+ssd_comps_to_plot);  % Bottom: SSD recovered alpha sources           
    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f', freq, n, '{\lambda}', lambda(n)));
    showfield(pattern_p(:,n), source_analysis.locs_2D);   
end

drawnow;
savefig([output_dir filename]);

end