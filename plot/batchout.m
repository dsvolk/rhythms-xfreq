function batchout ( str )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

global PIDX;
if isempty(PIDX)
    param_str = '';
else
    %param_str = sprintf('[PARAM %d]',PIDX);
    param_str = sprintf('[P=%d]',PIDX);
end

global TIDX;
if isempty(TIDX)
    trial_str = '';
else
    %trial_str = sprintf('[TRIAL %d] ',TIDX);
    trial_str = sprintf('[T=%d] ',TIDX);
end
disp([param_str trial_str '[' datestr(now, 'HH:MM:SS') '] Script: ' str]);

end

