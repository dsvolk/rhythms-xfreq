%% Starting the script
addpath(genpath('..'));     % add all same-level-directories to MATLAB search path
diary('diary.txt')
close all;

for PIDX = 1:28

filename = sprintf('[%d]',PIDX);
figure('name',filename,'NumberTitle','off'); plot(sin(1:50),'*');

end

autoArrangeFigures;