function figuresAsSubplots(NX, NY, monitor)
%
% INPUT :
% NX : number of grid cells in horizontal direction
% NY : number of grid cells in vertical direction
% monitor : monitor number (1 or 2) (optional, default 1)
% OUTPUT :
%
% How to use.
% Call this function once after all figures are opened.
%
% get every figures that are opened now and arrange them.
% if you want arrange automatically, just type 'autoArrangeFigures(0,0)'
% but maximum number of figures for automatic mode is 40.
%
% if you want specify size of grid, give non-zero numbers for parameters.
% but if your grid size is smaller than actually opened,
% this function will switch to automatic mode and if more
% figures are opend than maximum number, then it gives error.
%
% Example:
% 10 figures are opend, and you want to arrange them by 2 x 5.
% Then you can type 'autoArrangeFigures(2,5)'
%
% Original Version 1.0 by leejaejun, Koreatech, Korea Republic, 2014.10.19
% jaejun0201@koreatech.ac.kr
% source: http://www.mathworks.com/matlabcentral/fileexchange/48480-autoarrangefigures-nh--nw-
%
% Version 2.0 by Denis Volk, 2016.08.24
% dire.ulf@gmail.com

figHandle = flip(findobj('Type','figure'));

for k = 1:size(figHandle)
    print(figHandle(k),figHandle(k).Name,'-dpng')
end

return;


%figHandle = findobj('Type','figure');
n_fig = size(figHandle,1);
if n_fig <= 0
    error('Found no figures.');
end

if ~exist('NX', 'var')
    NX = 0;
end
if ~exist('NY', 'var')
    NY = 0;
end
if ~exist('monitor', 'var')
    monitor = 1;
end

N_FIG = NX * NY;
if N_FIG == 0
    autoArrange = true;
else
    if n_fig > N_FIG
        autoArrange = true;
        warning(sprintf('Found more figures than specified by parameters (%d > %d). Switching to auto mode.',  n_fig, N_FIG));
    else
        autoArrange = false;
    end
end

screen_sz = get(0,'MonitorPositions');
if monitor == 2 % WAT IS THIS???
    screen_sz(2,3) = screen_sz(2,3)-screen_sz(1,3);
    screen_sz(2,4) = screen_sz(1,4);
end

scn_width = screen_sz(monitor, 3);
scn_height = screen_sz(monitor, 4);


if autoArrange
    grid = [2 2 3 2 3 3 4 4 5 5 4 4 5 5 5 6 6 6 5 5 7 6 6 6 7 7 7 7 6 6 8 8 7 7 7 8 8 8 8 8; 
            1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 4 4 3 4 4 4 4 4 4 4 5 5 4 4 5 5 5 5 5 5 5 5]';
    
    if n_fig > size(grid,1)
        error(sprintf('Cannot handle that many figures in auto mode (maximum = %d). Please specify dimensions explicitly.', size(grid,1)));
    end
    
    %grid = [2 2 2 2 2 3 3 3 3 3 3 4 4 4 4 4 4 4 4 4;         3 3 3 3 3 3 3 3 4 4 4 5 5 5 5 5 5 5 5 6]';
    
    if scn_width > scn_height
        nx = grid(n_fig,1);
        ny = grid(n_fig,2);
    else
        nx = grid(n_fig,2);
        ny = grid(n_fig,1);
    end
else
    nx = NX;
    ny = NY;
end

fh = figure;
k=1;
for y = 1:ny
    for x = 1:nx
        ha = subplot(nx,ny,k);
               
        hc  = get(figHandle(k),'children');
        hgc = get(hc, 'children');
        copyobj(hgc,ha);
        k = k+1;
    end
end

offset_top = 24;
offset_bottom = 24;
offset_left = 10;

fig_size_h = scn_width-offset_left;
fig_size_v = (scn_height-offset_top-offset_bottom);

scr_left = screen_sz(monitor, 1);
scr_bottom = screen_sz(monitor, 2);

fig_pos = [scr_left+offset_left scn_height-offset_top fig_size_h fig_size_v];
set(fh,'OuterPosition',fig_pos);

end

