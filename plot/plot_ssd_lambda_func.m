function [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, freq, lambda, PairsNum, ssd_secondary_comp_num )

[fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );

fig.Name = sprintf('Fig. %d: SSD lambdas for %dHz', fig_idx, freq);
filename = sprintf('[Fig %d] SSD lambdas for %dHz', fig_idx, freq);
plot([1:length(lambda)], lambda, 'k*'); hold on; grid on; grid minor; xlim([1 length(lambda)]);
plot([PairsNum PairsNum], [0 1], 'b--');

if exist('pattern_sig','var')
    plot([ssd_secondary_comp_num ssd_secondary_comp_num], [0 1], 'b:');
    legend({'lambda','PairsNum','#SSD components to be used'});
else    
    legend({'lambda','PairsNum'});
end

hold off;

drawnow;
savefig([output_dir filename]);

end