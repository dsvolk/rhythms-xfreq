grid on; 
%set(gca,'XTickLabel',num2str(get(gca,'XTick').'));
%set(gca,'YTickLabel',num2str(get(gca,'YTick').'));
ylabel('* 2\pi')

%ax = gca;
%ax.YTick = [0:pi:10000*pi];
%ax.YTickLabel = {'0','\pi/4','\pi/2','3\pi/4','\pi','5\pi/4','3\pi/2','7\pi/4','2\pi'};

h1 = zoom;
h2 = pan;
set(gca,'XTickLabelMode','auto');
set(gca, 'XTickLabel', num2str(get(gca,'XTick').'))
set(gca,'YTickLabelMode','auto');
set(gca, 'YTickLabel', num2str(get(gca,'YTick').'))
set(h1,'ActionPostCallback',@mypostcallbackXY);
set(h2,'ActionPostCallback',@mypostcallbackXY);