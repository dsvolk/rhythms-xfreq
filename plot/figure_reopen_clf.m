function [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx )

fig_idx = fig_idx+1;

if ~exist('fig_handles','var') || (~isgraphics(fig_handles(fig_idx),'figure'))
    h = figure('NumberTitle','off');
    fig_handles(fig_idx) = h;    
else
    figure(fig_handles(fig_idx)); clf;
end

fig = fig_handles(fig_idx);

end