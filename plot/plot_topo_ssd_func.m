function [fig_idx] = plot_topo_ssd_func ( fig_handles, fig_idx, output_dir, freq, lambda, pattern_p, PairsNum, GoodMnt2D )

[fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );

fig.Color = 'white';
colormap('jet');
fig.Name = sprintf('SSD %dHz sources (normalized individually)', freq);
filename = fig.Name;
for n=1:PairsNum
    subplot(1,PairsNum,n);   % SSD recovered alpha sources
    ppp = pattern_p(:,n);
    rangescalp = [ min(ppp) max(ppp)];
    plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', rangescalp, 'Shading', 'interp', 'Extrapolation', 1);
    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f', freq, n, '{\lambda}', lambda(n)));
end

drawnow;
savefig([output_dir filename]);

end