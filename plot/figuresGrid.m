function [cols, rows] = figuresGrid ( n_fig )

    grid = [1 2 3 2 3 3 4 4 5 5 4 4 5 5 5 6 6 6 5 5 7 6 6 6 7 7 7 7 6 6 8 8 7 7 7 8 8 8 8 8;
            1 1 1 2 2 2 2 2 2 2 3 3 3 3 3 3 3 3 4 4 3 4 4 4 4 4 4 4 5 5 4 4 5 5 5 5 5 5 5 5]';
        
    if n_fig > size(grid,1)
        error(sprintf('Cannot handle that many figures in auto mode (maximum = %d).', size(grid,1)));
    end
    
    cols = grid(n_fig,1);
    rows = grid(n_fig,2);    
end