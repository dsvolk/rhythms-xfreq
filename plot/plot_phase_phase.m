function [fh] = plot_phase_phase (angle1, angle2, title_str, xlabel_str, ylabel_str, fr1, fr2)

dat = [mod(angle1, 2*pi), mod(angle2, 2*pi)];
fh = figure;
hist3(dat,'Edges', {[0:pi/8:2*pi], [0:pi/8:2*pi]});
decorate_phase_phase;

if (~exist('label1', 'var'))
    xlabel(xlabel_str);
end

if (~exist('label2', 'var'))
    ylabel(ylabel_str);
end

if (~exist('title', 'var'))
    title(title_str);
end

if (~exist('fr1', 'var')) && (~exist('fr2', 'var'))
    phase_rel =  mod(fr2*angle1 - fr1*angle2, 2*pi);
    legend(sprintf('Kuramoto = %.4f', abs(mean(exp(1i.*phase_rel)))));
end


