%% Generalized Cross-Frequency Decomposition (realistic, MNI-based) Head Simulation script
%% Denis VOLK, Centre for Cognition & Decision Making, National Research University Higher School of Economics, Moscow, Russia

addpath(genpath('..'));         % add all same-level-directories to MATLAB search path
addpath('/home/sasha/Desktop/cognition/Task2/BCI_tools');
output_dir = ['./out_test_dev/' datestr(now,'yymmdd_HHMMSS') '/'];
mkdir(output_dir);
diary([output_dir 'diary.txt']);
close all;
clearvars -except 'rng_saved_states' 'output_dir';
rng('shuffle','twister');
rng_seed_default = rng();

%% Set up the parameters
mk_clabs;
gcfd_test_dev_ctrl;
plotting_on = false;
reproducibility_on = true;
for PIDX = 1:size(param, 2)
    param(PIDX).PairsNum = 5;
    param(PIDX).SourceNum = param(PIDX).PairsNum * 2;
    param(PIDX).randomize_filter_seeds = false;
end

%% Main script
gcfd_test_batch;



