function [err_lin, err_nonlin] = testRandModelMNI(seed_pat_all, seed_src_sig, seed_src_noise, ...
                                ssd_primary_on, ssd_secondary_on, randomize_filter_seeds, ...
                                frP, frQ, Secs, PairsNum, NoiseNum, SNR)
    rng_seed_default = 1;
    mk_clabs;
    gcfd_test_dev_ctrl;
    [source_analysis, GoodMnt2D, GoodChannels] = load_chan_info (default);
    sound_on_finish = false;
    
    PIDX = 1;
    param(PIDX).FwdModel = FwdModelType.GuidoMNI;
    param(PIDX).seed_pat_all = seed_pat_all;
    param(PIDX).seed_src_sig = seed_src_sig;
    param(PIDX).seed_src_noise = seed_src_noise;
    param(PIDX).Secs = Secs;
    param(PIDX).PairsNum = PairsNum;
    param(PIDX).NoiseNum = NoiseNum;
    param(PIDX).FrP = frP;
    param(PIDX).FrQ = frQ;
    param(PIDX).SNR = SNR;
    param(PIDX).ssd_primary_on = ssd_primary_on;
    param(PIDX).ssd_secondary_on = ssd_secondary_on;
    param(PIDX).randomize_filter_seeds = randomize_filter_seeds;
    param(PIDX).filter_seed_ssd = false;
    param(PIDX).xtest_pq = 0;
    param(PIDX).use_plotting = false;
    %param(PIDX).targets_abs_1 = true;
    param(PIDX).SourceNum = param(PIDX).PairsNum * 2;

    %{
    new_lsqnonlin_param.Algorithm = 'levenberg-marquardt';
    new_lsqnonlin_param.Display = 'final-detailed';
    new_lsqnonlin_param.TolFun = 1e-8;
    new_lsqnonlin_param.MaxFunEvals = 1000;
    new_lsqnonlin_param.MaxIter = 100;
    %new_lsqnonlin_param.PlotFcn = {@optimplotx, @optimplotfunccount, @optimplotfval, @optimplotresnorm, @optimplotstepsize, @optimplotfirstorderopt};
    new_lsqnonlin_param.PlotFcn = [];
    if frP > 1
        new_lsqnonlin_param.TolFun = 1e-6;
        new_lsqnonlin_param.MaxFunEvals = 1000000;
        new_lsqnonlin_param.MaxIter = 1000;
    end
    param(PIDX).lsqnonlin_param = new_lsqnonlin_param;
    %}
    %p = param(PIDX)
    TIDX = 1;

    %% create band-pass filters
    [butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a] = ...
        mk_bp_filters ( param(PIDX).FrP, param(PIDX).FrQ, param(PIDX).FrBase, param(PIDX).Discr );

    [elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q] = mk_elec (source_analysis, GoodMnt2D, param(PIDX), ...
            butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a);
    
    %% Preprocess the raw data
    [butter_preproc_b, butter_preproc_a] = butter(2,[default.fr_cutoff_low default.fr_cutoff_high]/(default.Discr/2));
    elec = filtfilt (butter_preproc_b, butter_preproc_a, elec);
    
    [PRE_1_ssd_primary(TIDX,:), PRE_1_ssd_secondary(TIDX,:), PRE_lin(TIDX,:), PRE_1_final(TIDX,:), fig_idx] = ...
        gcfd_test ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
             butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a, ...
             source_analysis, GoodMnt2D, param(PIDX), ...
             0, 0, 0, false);
end