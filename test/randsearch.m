clear variables;
addpath(genpath('..'));

N = 1;

lin_nonlin_err_diff_vec = zeros(1,N);
print = 1;
params = {
    %      1         2         3         4      5     6        7      8      9  10   11      12    13    14  15 
    %seed_ps   seed_pn   seed_ss   seed_sn  e_lin   e_nonlin  print  ssd_on  p   q time sensors pairs noise snr
    {      1,        2,        3,        4, 0.30,      0.30,  print,     1,  3,  1, 152,    64,    2,  100, 0.1}
};

rng('shuffle');
for i = 1:N
    seed_pat_sig   = randi([0 9999999]);
    seed_pat_noise = randi([0 9999999]);
    seed_src_sig   = randi([0 9999999]);
    seed_src_noise = randi([0 9999999]);
    err_lin_lim    = cell2mat(params{1}(5));
    err_nonlin_lim = cell2mat(params{1}(6));
    print          = cell2mat(params{1}(7));
    ssd_on         = cell2mat(params{1}(8));
    frP            = cell2mat(params{1}(9));
    frQ            = cell2mat(params{1}(10));
    Time           = cell2mat(params{1}(11));
    SensorsNum     = cell2mat(params{1}(12));
    PairsNum       = cell2mat(params{1}(13));
    NoiseNum       = cell2mat(params{1}(14));
    snr            = cell2mat(params{1}(15));
    fprintf('test #%d\n', i);
    fprintf('you can reproduce test:\n');
    fprintf('testRandModel(%d,%d,%d,%d,%.2f,%.2f,%d,%d,%d,%d,%d,%d,%d,%d,%.1f)\n', ...
             seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
             err_lin_lim, err_nonlin_lim, print, ssd_on, ...
             frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    [fail, err_lin, err_nonlin] = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                         err_lin_lim, err_nonlin_lim, print, ssd_on, ...
                         frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    lin_nonlin_err_diff_vec(i) = max(abs(err_lin - err_nonlin));
    if fail || max(abs(err_lin - err_nonlin)) > 0.30
        fprintf('FAILED!\n\n\n');
        return;
    else
        fprintf('PASSED\n\n\n');
    end
end