clear variables;
addpath(genpath('..'));

N = 10;

print = 1;
params = {
    %      1         2         3         4      5     6        7      8  9  10   11      12    13    14  15 
    %seed_ps   seed_pn   seed_ss   seed_sn  error err_dist print ssd_on  p   q time sensors pairs noise snr
    {2816397,  4108660,  6093512,  1702277, 0.03,   0.005, print,   0,   1,  2, 152,    15,    1,  100, 0.1}
};

i = 1;
    
error          = cell2mat(params{1}(5));
error_dist     = cell2mat(params{1}(6));
print          = cell2mat(params{1}(7));
ssd_on         = cell2mat(params{1}(8));
frP            = cell2mat(params{1}(9));
frQ            = cell2mat(params{1}(10));
Time           = cell2mat(params{1}(11));
SensorsNum     = cell2mat(params{1}(12));
PairsNum       = cell2mat(params{1}(13));
NoiseNum       = cell2mat(params{1}(14));
snr            = cell2mat(params{1}(15));
    
fails_rand_pat_sig   = 0;
fails_rand_pat_noise = 0;
fails_rand_src_sig   = 0;
fails_rand_src_noise = 0;

for j = 1:N
    rng('shuffle');
    seed_pat_sig   = randi([0 9999999]);
    seed_pat_noise = cell2mat(params{i}(2));
    seed_src_sig   = cell2mat(params{i}(3));
    seed_src_noise = cell2mat(params{i}(4));
    fprintf('test #%d\n', i);
    fprintf('you can reproduce test:\n');
    fprintf('testRandModel(%d,%d,%d,%d,%.2f,%.3f,%d,%d,%d,%d,%d,%d,%d,%d,%.1f)\n', ...
             seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
             error, error_dist, print, ssd_on, ...
             frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    fail = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                         error, error_dist, print, ssd_on, ...
                         frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
        if fail
            fprintf('FAILED!\n\n\n');
            fails_rand_pat_sig = fails_rand_pat_sig + 1;
        else
            fprintf('PASSED\n\n\n');
        end
end
for j = 1:N
    rng('shuffle');
    seed_pat_sig   = cell2mat(params{i}(1));
    seed_pat_noise = randi([0 9999999]);
    seed_src_sig   = cell2mat(params{i}(3));
    seed_src_noise = cell2mat(params{i}(4));
    fprintf('test #%d\n', i);
    fprintf('you can reproduce test:\n');
    fprintf('testRandModel(%d,%d,%d,%d,%.2f,%.3f,%d,%d,%d,%d,%d,%d,%d,%d,%.1f)\n', ...
             seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
             error, error_dist, print, ssd_on, ...
             frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    fail = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                         error, error_dist, print, ssd_on, ...
                         frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
        if fail
            fprintf('FAILED!\n\n\n');
            fails_rand_pat_noise = fails_rand_pat_noise + 1;
        else
            fprintf('PASSED\n\n\n');
        end
end
for j = 1:N
    rng('shuffle');
    seed_pat_sig   = cell2mat(params{i}(1));
    seed_pat_noise = cell2mat(params{i}(2));
    seed_src_sig   = randi([0 9999999]);
    seed_src_noise = cell2mat(params{i}(4));
    fprintf('test #%d\n', i);
    fprintf('you can reproduce test:\n');
    fprintf('testRandModel(%d,%d,%d,%d,%.2f,%.3f,%d,%d,%d,%d,%d,%d,%d,%d,%.1f)\n', ...
             seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
             error, error_dist, print, ssd_on, ...
             frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    fail = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                         error, error_dist, print, ssd_on, ...
                         frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
        if fail
            fprintf('FAILED!\n\n\n');
            fails_rand_src_sig = fails_rand_src_sig + 1;
        else
            fprintf('PASSED\n\n\n');
        end
end
for j = 1:N
    rng('shuffle');
    seed_pat_sig   = cell2mat(params{i}(1));
    seed_pat_noise = cell2mat(params{i}(2));
    seed_src_sig   = cell2mat(params{i}(3));
    seed_src_noise = randi([0 9999999]);
    fprintf('test #%d\n', i);
    fprintf('you can reproduce test:\n');
    fprintf('testRandModel(%d,%d,%d,%d,%.2f,%.3f,%d,%d,%d,%d,%d,%d,%d,%d,%.1f)\n', ...
             seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
             error, error_dist, print, ssd_on, ...
             frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
    fail = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                         error, error_dist, print, ssd_on, ...
                         frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr);
        if fail
            fprintf('FAILED!\n\n\n');
            fails_rand_src_noise = fails_rand_src_noise + 1;
        else
            fprintf('PASSED\n\n\n');
        end
end
fprintf('result:\n');
fprintf('fails_rand_pat_sig:   %d\n', fails_rand_pat_sig);
fprintf('fails_rand_pat_noise: %d\n', fails_rand_pat_noise);
fprintf('fails_rand_src_sig:   %d\n', fails_rand_src_sig);
fprintf('fails_rand_src_noise: %d\n', fails_rand_src_noise);

