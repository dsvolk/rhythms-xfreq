function [fail, err_lin, err_nonlin] = testRandModel(seed_pat_sig, seed_pat_noise, seed_src_sig, seed_src_noise, ...
                                err_lin_lim, err_nonlin_lim, print, ssd_on, ...
                                frP, frQ, Time, SensorsNum, PairsNum, NoiseNum, snr)
    rng_seed_default = 1;
    mk_clabs;
    gcfd_test_dev_ctrl;
    source_analysis = [];
    GoodMnt2D = [];
    sound_on_finish = false;
    
    PIDX = 1;
    param(PIDX).FwdModel = FwdModelType.Rand;
    param(PIDX).seed_pat_sig = seed_pat_sig;
    param(PIDX).seed_pat_noise = seed_pat_noise;
    param(PIDX).seed_src_sig = seed_src_sig;
    param(PIDX).seed_src_noise = seed_src_noise;
    param(PIDX).Secs = Time;
    param(PIDX).PairsNum = PairsNum;
    param(PIDX).NoiseNum = NoiseNum;
    param(PIDX).SensorsNum = SensorsNum;
    param(PIDX).FrP = frP;
    param(PIDX).FrQ = frQ;
    param(PIDX).SNR = snr;
    param(PIDX).ssd_primary_on = ssd_on;
    param(PIDX).ssd_secondary_on = ssd_on;
    param(PIDX).filter_seed_ssd = false;
    param(PIDX).xtest_pq = 0;
    param(PIDX).use_plotting = false;
    %param(PIDX).targets_abs_1 = true;
    param(PIDX).SourceNum = param(PIDX).PairsNum * 2;

    new_lsqnonlin_param.Algorithm = 'levenberg-marquardt';
    new_lsqnonlin_param.Display = 'final-detailed';
    new_lsqnonlin_param.TolFun = 1e-8;
    new_lsqnonlin_param.MaxFunEvals = 1000;
    new_lsqnonlin_param.MaxIter = 100;
    %new_lsqnonlin_param.PlotFcn = {@optimplotx, @optimplotfunccount, @optimplotfval, @optimplotresnorm, @optimplotstepsize, @optimplotfirstorderopt};
    new_lsqnonlin_param.PlotFcn = [];
    if frP > 1
        new_lsqnonlin_param.TolFun = 1e-6;
        new_lsqnonlin_param.MaxFunEvals = 1000000;
        new_lsqnonlin_param.MaxIter = 1000;
    end
    param(PIDX).lsqnonlin_param = new_lsqnonlin_param;

    %p = param(PIDX)
    if frP == 1
        assert(err_lin_lim >= err_nonlin_lim);
    end
    TIDX = 1;
    %% create band-pass filters
    [butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a] = ...
        mk_bp_filters ( FrP, FrQ, param(PIDX).FrBase, param(PIDX).Discr );

    [elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q] = mk_elec (source_analysis, GoodMnt2D, param(PIDX), ...
            butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a);
    
    %% Preprocess the raw data
    [butter_preproc_b, butter_preproc_a] = butter(2,[default.fr_cutoff_low default.fr_cutoff_high]/(default.Discr/2));
    elec = filtfilt (butter_preproc_b, butter_preproc_a, elec);
    
    f0_norm = 0;
    step = 0;
    if frP > 1
        f0_norm = 1;
        assert(SensorsNum == 15 || SensorsNum == 64);
        if SensorsNum == 15
            step = 3;
        end
        if SensorsNum == 64
            step = 16;
        end
        if param(PIDX).ssd_secondary_on
            step = 3;
        end
    end

    [PRE_1_ssd_primary(TIDX,:), PRE_1_ssd_secondary(TIDX,:), PRE_lin(TIDX,:), PRE_1_final(TIDX,:), fig_idx] = ...
        gcfd_test ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
             butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a, ...
             source_analysis, GoodMnt2D, param(PIDX), ...
             0, 0, 0, false);
    if print
        batchout(sprintf('Pattern recovery error:'));
        disp(PRE_lin);
        disp(PRE_1_final);
    end
    if frP == 1 && PRE_1_final > err_nonlin_lim%PRE_lin
        batchout(sprintf('Rerun (try to improve nonlin result):'));
        [PRE_1_ssd_primary(TIDX,:), PRE_1_ssd_secondary(TIDX,:), PRE_lin(TIDX,:), PRE_1_final(TIDX,:), fig_idx] = ...
        gcfd_test ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
             butter_p_b, butter_p_a, butter_q_b, butter_q_a, ...
             source_analysis, GoodMnt2D, param(PIDX), ...
             [], [], 0, 1, f0_norm, step);
    end
    if print
        batchout(sprintf('Pattern recovery error:'));
        disp(PRE_lin);
        disp(PRE_1_final);
    end
    %if frP == 1
    %    assert(PRE_1_final <= PRE_lin);
    %end
    err_lin = abs(PRE_lin);
    err_nonlin = abs(PRE_1_final);
    fail = false;
    if max(err_lin) > err_lin_lim || max(err_nonlin) > err_nonlin_lim
        fail = true;
    end
end