%% Generalized Cross-Frequency Decomposition (realistic, MNI-based) Head Simulation script
%% Denis VOLK, Centre for Cognition & Decision Making, National Research University Higher School of Economics, Moscow, Russia

% TODO: try running on longer time frames (say, 5 min)
% TODO: try running with better Discr (say, 1000)
% TODO: SSD dimensionality reduction option for FrQ
% TODO: same for FrP - to replace PairsNum as a strict upper limit for components

% TODO: try parpool, parfor

% TODO: see other ??? in the code

warning('on','MATLAB:RandStream:ActivatingLegacyGenerators')
warning('on','MATLAB:RandStream:ReadingInactiveLegacyGeneratorState') 

%% Setup the environment
warning('off','MATLAB:dispatcher:pathWarning'); % suppress warnings about non-existent folders

addpath(genpath('..'));         % add all same-level-directories to MATLAB search path

addpath('/Users/1/MATLAB_Toolboxes/Guido_toolbox/');    % for Volkobook
addpath('/Users/1/MATLAB_Toolboxes/bbci_public/');      % Berlin BCI for Volkobook
%addpath('/Users/1/MATLAB_Toolboxes/eeglab13_6_5b/');    % EEGLab for Volkobook
addpath('/Users/1/MATLAB_Data/Zafer_Rest17');

addpath('C:/MATLAB_Toolboxes/Guido_toolbox/');   % for Denis's Windows machines
addpath('C:/MATLAB_Toolboxes/bbci_public/');     % Berlin BCI for Denis's Windows machines
addpath('C:/MATLAB_Data/Zafer_Rest17');

% Sasha, Daniil, please add your path strings here

warning('on','MATLAB:dispatcher:pathWarning');

startup_bbci_toolbox;

output_dir = './out_test_volkobook/';
mkdir(output_dir);

%% Preparing to start the script
diary([output_dir 'diary.txt']);
close all;
clearvars -except 'rng_saved_states' 'output_dir';
disp('================================================================================');
disp('==== gCFD SSD+(optional SSD)+gCFD ==============================================');
disp('================================================================================');
global PIDX;    % iterator over various parameter settings
global TIDX;    % iterator over trials within each PIDX
rng('shuffle','twister');
rng_seed_default = rng();

stdout([datestr(now,'dd/mm/yyyy') '. Ready to rock now!']);
disp('[Testground Volkobook]');
disp('This run is about:');
disp('Debugging the script.');
ScriptCPUStart = cputime;

%% Set up the parameters
gcfd_head_sim_ct_volkobook;

%% Main script
gcfd_head_sim;

%% Finishing the script
ScriptCPUFinish = cputime;
stdout(['Finished, taking ' sprintf('%.2f',ScriptCPUFinish-ScriptCPUStart) ' secs of CPU time.']);
diary off;

%% Play a sound
%if sound_on_finish
    [snd,Fs] = audioread('batch_script_complete.m4a');
    sound(snd,Fs);
%end

email_denis(['[MATLAB][Volkobook] gcfd_head_sim.m has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.'],['The script "gcfd_head_sim.m" has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.']);
return;

