%% [script control] experiment parameters

% default parameters for experiments
default.Discr = 200;        % time discretization, Hz
default.Secs = 152;         % overall signal time, sec
%default.Secs = 302;

FWD_MODEL_GUIDO_MNI64 = 1;
FWD_MODEL_RAND = 2;
FWD_MODEL_DATA = 3;

rng_seed = rng_seed_default;    % new every run, based on the current time
%rng_seed = rng_saved_states(1);        % choose from manually pre-loaded seeds

default.FwdModel = FWD_MODEL_GUIDO_MNI64;  % choose one of the above options

default.PairsNum = 5;       % number of synced source pairs
default.NoiseNum = 100;       % number of (pink) noise sources

default.SensorsNum = 64;     % number of sensors (electrodes etc.)

default.FrBase = 10;    % base frequency, Hz
default.FrP = 2;        % frequency ratio - (where the nonlinearity hides)
default.FrQ = 3;        % frequency ratio - (harmless)

default.batch_size = 100;

default.normalize_sources = false;
default.targets_abs_1 = false;

default.ampli_flip = false;

default.compute_snr = true;
default.normalize_snr = true;
default.snr = 1.0;

default.ssd_primary_on = true;  % you can disable it in FWD_MODEL_RAND or FWD_MODEL_GUIDO_MNI64 modes
default.ssd_secondary_on = false;  % you can disable it in all modes

default.ssd_secondary_comp_num = Inf;    % set to Inf to use all the ssd_q components

default.ssd_filter_offset_start = 200;
default.ssd_filter_offset_end = 200;
default.plot_ssd_lambda = false;

default.randomize_filter_seeds = false;
default.filter_seeds_num = 1;
default.filter_seed_cfd2012 = true;

default.lsqnonlin_param.Algorithm = 'levenberg-marquardt';
default.lsqnonlin_param.Display = 'final-detailed';
default.lsqnonlin_param.TolFun = 1e-6;
default.lsqnonlin_param.MaxFunEvals = 1000000;

default.pre_debug_thresh = 2;    % Possible values: 0..1. Set 2 to disable threshold

default.plot_trials_as_lines = true;
default.plot_ssd_error_hist = true;
default.plot_dual_pre_ssd_error_hist = true;
plot_pre_as_heatmap = true;

default.source_topo_figures = false;
default.calc_sync_measures = false;

sound_on_finish = false;

%% PARAMS 
%PARAMS = 1:12;
PARAMS = 1:6;
param(PARAMS) = default;

% For the composite PRE figure
[Comp_PRE_Fig_Cols, Comp_PRE_Fig_Rows] = figuresGrid(length(PARAMS));

%% set non-default parameters
% Original 12
%FrP = [1 2 1 3 2 3 3 4 2 5 3 5];
%FrQ = [2 1 3 1 3 2 4 3 5 2 5 3];

% Just a single parameter
%FrP = [1];
%FrQ = [4];

% For experimental_01
%FrP = [1 1];
%FrQ = [3 4];

% [21] '5.21' All combinations up to 5
%FrP = [1 1 2 1 3 2 3 1 4 2 4 3 4 1 5 2 5 3 5 4 5];
%FrQ = [1 2 1 3 1 3 2 4 1 4 2 4 3 5 1 5 2 5 3 5 4];

% [18] 'R.18' Realistic: slow Ref and fast Src
%FrP = [1 2 3 3 4 4 5 5 5 5 6 6 7 7 7 7 7 7];
%FrQ = [1 1 1 2 1 3 1 2 3 4 1 5 1 2 3 4 5 6];

% [10] 'R.10' Realistic: slow Ref and fast Src
% FrP = [1 2 3 3 4 4 5 5 5 5];
% FrQ = [1 1 1 2 1 3 1 2 3 4];

% [1] 'R.1' Benchmark for comparison w m_n_synchronization_simulation script by Vadim Nikulin
% FrP = [1];
% FrQ = [2];

% [18] 'R.18' Realistic: slow Ref and fast Src
% FrP = [1 2 3 3 4 4 5 5 5 5 6 6 7 7 7 7 7 7];
% FrQ = [2 1 1 2 1 3 1 2 3 4 1 5 1 2 3 4 5 6];

% [4] 'R.4' First batch tests with a realistic forward model
% FrP = [1 2 2 3];
% FrQ = [3 1 3 2];

% [6] 'R.6' First batch tests with a realistic forward model
FrP = [1 1 2 2 3 3];
FrQ = [2 3 1 3 1 2];

% [1] 'R.1' Benchmark for comparison w m_n_synchronization_simulation script by Vadim Nikulin
%FrP = [2];
%FrQ = [1];

for j = PARAMS
    param(j).FrP = FrP(j);
    param(j).FrQ = FrQ(j);
end