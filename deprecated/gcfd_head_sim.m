%% Load GoodChannels and source_analysis/montage data
load_chan_info;

%% Saving the percentages of good recoveries
pre_good_percent = NaN (max(FrP(PARAMS)), max(FrQ(PARAMS)));
fn_pre = sprintf('Pattern Recovery Error - all params');
fh_pre = figure('Name',fn_pre,'NumberTitle','off');

%% Launch the Random Number Generator
rng(rng_seed);

%% Parameters' for loop
pidx = 1;   % in case PARAMS is not from 1 to N, this is an iterator starting at 1
stdout(sprintf('Multi-batch mode with %d batches.', length(PARAMS)));
for PIDX = PARAMS
    
    %% copy ALL (!) param values for brevity of code
    copy_param;
    
    %% [script control] time window
    % SecBegin = 50;      % time window start, sec
    % SecEnd = 75;        % time window end, sec
    % TShow = [SecBegin*Discr+1:SecEnd*Discr];
    
    TShow = [1:T];
    
    %% [initializing utility constants]
    Time = 1./Discr*[1:T];
    TShowDiff = [TShow(1):TShow(end)-1];
    
    SourceNum = PairsNum * 2;
    
    %% Main part
    PRE_ssd = NaN(batch_size,PairsNum);
    PRE_ssd_q = NaN(batch_size,PairsNum);
    PRE_final = NaN(batch_size,PairsNum);
    
    clearvars 'rng_states';
    
    stdout(sprintf('Batch %d of %d started.', PIDX, length(PARAMS)));
    stdout('Param = ');
    disp(param(PIDX));
    for TIDX = 1:batch_size
        
        fig_idx = 1;
        fig_handles = gobjects(100,1);
        
        rng_states(TIDX) = rng();
        
        batchout('Yiihaaa!.. another trial.');
        
        %% Make up or load the electrode recordings
        mk_elec;
        
        %% Now we attempt to decompose them back:
        %% SSD DECOMPOSITION PART:
        
        if ssd_primary_on
            batchout('SSD for FrP components...');
            %% run SSD to recover FrP sources
            %[ X_ssd, A ] = spatio_spectral_decomposition( elec, Discr, ssd_filter_offset_start, ssd_filter_offset_end, FrP*FrBase-1, FrP*FrBase+1, FrP*FrBase-3, FrP*FrBase+3 );
            [W, pattern_p, lambda, C_s, X_ssd_p] = ssd(elec, [FrP*FrBase-1 FrP*FrBase+1; FrP*FrBase-3 FrP*FrBase+3; FrP*FrBase-2 FrP*FrBase+2], Discr, 2, [1 T]);
            batchout('Done.');
            
            if plot_ssd_lambda
                %%
                [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
                fig.Name = sprintf('Fig. %d: SSD lambdas for %dHz', fig_idx-1, FrP*FrBase);
                plot([1:length(lambda)], lambda, 'k*'); hold on; grid on; grid minor; xlim([1 length(lambda)]);
                plot([PairsNum PairsNum], [0 1], 'b--');
                legend({'lambda','PairsNum'});
                hold off;
            end
            
            if FwdModel == FWD_MODEL_DATA
                %if source_topo_figures
                %% plotting recovered FrP sources
                fig_handles(fig_idx) = figure_reopen_clf (fig_handles(fig_idx)); fig_idx = fig_idx+1;
                fig.Color = 'white';
                colormap('jet');
                fig.Name = sprintf('SSD %dHz sources (normalized individually)', FrP*FrBase);
                for n=1:PairsNum
                    subplot(1,PairsNum,n);   % SSD recovered alpha sources
                    ppp = pattern_p(:,n);
                    rangescalp = [ min(ppp) max(ppp)];
                    plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', rangescalp, 'Shading', 'interp', 'Extrapolation', 1);
                    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f', FrP*FrBase, n, '{\lambda}', lambda(n)));
                end
                %end
            end
            
            if (FwdModel == FWD_MODEL_RAND) || (FwdModel == FWD_MODEL_GUIDO_MNI64)
                
                [ pat_sig_p_idx, pat_p_idx, PRE_ssd(TIDX,:) ] = match_patterns_Volk2 ( pattern_sig_p, pattern_p(:,1:PairsNum), PairsNum );
                
                batchout(sprintf('SSD for FrP pattern recovery error:'));
                disp(PRE_ssd(TIDX,:));
            end
            
            if FwdModel == FWD_MODEL_GUIDO_MNI64
                
                if source_topo_figures
                    %% plotting recovered FrP sources
                    [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
                    fig.Color = 'white';
                    colormap('jet');
                    fig.Name = sprintf('Fig. %d: SSD %dHz sources', fig_idx-1, FrP*FrBase);
                    for n=1:PairsNum
                        subplot(2,PairsNum,n);   % Top: true alpha sources
                        title(sprintf('True %dHz #%d', FrP*FrBase, pat_sig_p_idx(n)));
                        showfield(pattern_sig_p(:,pat_sig_p_idx(n)), source_analysis.locs_2D);
                        
                        subplot(2,PairsNum,n+PairsNum);  % Bottom: SSD recovered alpha sources
                        title(sprintf('SSD %dHz #%d\nSSD %s = %.4f\nError = %.4f', FrP*FrBase, pat_p_idx(n), '{\lambda}', lambda(pat_p_idx(n)),  PRE_ssd(TIDX,n)));
                        showfield(pattern_p(:,pat_p_idx(n)), source_analysis.locs_2D);
                    end
                end
            end
            
        else          
            if FwdModel == FWD_MODEL_DATA
                batcherr('SSD is off but this makes no sense in FWD_MODEL_DATA mode.');  
                return;
            end
            
            batchout('SSD is off. Instead, will be using the ''true'' FrP patterns and ''true'' signals for gCFD.');
            pattern_p = pattern_sig_p;
            X_ssd_p = src_sig_p;
        end
              
        if ssd_secondary_on
            batchout('SSD for FrQ components...');
            %% run SSD to recover FrQ sources
            [W_q, pattern_q_ssd, lambda, C_s, X_ssd_q] = ssd(elec, [FrQ*FrBase-1 FrQ*FrBase+1; FrQ*FrBase-3 FrQ*FrBase+3; FrQ*FrBase-2 FrQ*FrBase+2], Discr, 2, [1 T]);
            batchout('Done.');
            
            if plot_ssd_lambda
                %%
                [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
                fig.Name = sprintf('Fig. %d: SSD lambdas for %dHz', fig_idx-1, FrQ*FrBase);
                plot([1:length(lambda)], lambda, 'k*'); hold on; grid on; grid minor; xlim([1 length(lambda)]);
                plot([PairsNum PairsNum], [0 1], 'b--');
                plot([ssd_secondary_comp_num ssd_secondary_comp_num], [0 1], 'b:');
                legend({'lambda','PairsNum','#SSD components to be used'});
                hold off;
            end
            
            if FwdModel == FWD_MODEL_DATA
                %if source_topo_figures
                %% plotting recovered FrQ sources
                fig_handles(fig_idx) = figure_reopen_clf (fig_handles(fig_idx)); fig_idx = fig_idx+1;
                fig.Color = 'white';
                colormap('jet');
                fig.Name = sprintf('SSD %dHz sources (normalized individually)', FrQ*FrBase);
                for n=1:PairsNum
                    subplot(1,PairsNum,n);   % SSD recovered alpha sources
                    ppp = pattern_q_ssd(:,n);
                    rangescalp = [ min(ppp) max(ppp)];
                    plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', rangescalp, 'Shading', 'interp', 'Extrapolation', 1);
                    title(sprintf('SSD %dHz #%d\nSSD %s = %.4f', FrQ*FrBase, n, '{\lambda}', lambda(n)));
                end
                %end
            end
            
            if (FwdModel == FWD_MODEL_RAND) || (FwdModel == FWD_MODEL_GUIDO_MNI64)
                
                [ pat_sig_q_ssd_idx, pat_q_ssd_idx, PRE_ssd_q(TIDX,:) ] = match_patterns_Volk2 ( pattern_sig_q, pattern_q_ssd(:,1:PairsNum), PairsNum );
                
                batchout(sprintf('SSD for FrQ pattern recovery error:'));
                disp(PRE_ssd_q(TIDX,:));
            end
            
            if FwdModel == FWD_MODEL_GUIDO_MNI64
                
                if source_topo_figures
                    %% plotting recovered FrQ sources
                    [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
                    fig.Color = 'white';
                    colormap('jet');
                    fig.Name = sprintf('Fig. %d: SSD %dHz sources', fig_idx-1, FrQ*FrBase);
                    for n=1:PairsNum
                        subplot(2,PairsNum,n);   % Top: true alpha sources
                        title(sprintf('True %dHz #%d', FrQ*FrBase, pat_sig_q_ssd_idx(n)));
                        showfield(pattern_sig_q(:,pat_sig_q_ssd_idx(n)), source_analysis.locs_2D);
                        
                        subplot(2,PairsNum,n+PairsNum);  % Bottom: SSD recovered alpha sources
                        title(sprintf('SSD %dHz #%d\nSSD %s = %.4f\nError = %.4f', FrQ*FrBase, pat_q_ssd_idx(n), '{\lambda}', lambda(pat_q_ssd_idx(n)),  PRE_ssd_q(TIDX,n)));
                        showfield(pattern_q_ssd(:,pat_q_ssd_idx(n)), source_analysis.locs_2D);
                    end
                end
            end
            
        else          
%             if FwdModel == FWD_MODEL_DATA
%                 batcherr('SSD is off but this makes no sense in FWD_MODEL_DATA mode.');  
%                 return;
%             end
            
            batchout('SSD for FrQ is off. Running gCFD on raw channels.');
            
            pattern_p = pattern_sig_p;
            X_ssd_p = src_sig_p;
        end
        
        
        batchout('gCFD...');        
        %% genCFD DECOMPOSITION PART:  
        %% prepare the signals
        gcfd_elec_q_num = NaN;
        if ssd_secondary_on
            gcfd_elec_q_num = min([size(X_ssd_q,2) ssd_secondary_comp_num]);            
            gcfd_elec_q = X_ssd_q(:,1:gcfd_elec_q_num);
        else
            gcfd_elec_q_num = size(elec,2);
            gcfd_elec_q = filtfilt(butter_q_b,butter_q_a,elec);
        end
        
        gcfd_elec_q_num
        
        gcfd_elec_q = detrend(gcfd_elec_q,'constant');
        X_ssd_p = detrend(X_ssd_p,'constant');
        
        %% complexify the signals (compute 'analytic signals')
        m = hilbert(gcfd_elec_q);
        r = hilbert(X_ssd_p);
        
        %% frequency warp the SSD component
        if targets_abs_1
            r = exp(1i.*(FrQ*angle(r)));
        else
            r = abs(r).*exp(1i.*(FrQ*angle(r)));
        end
        
        filter_q = NaN(SensorsNum,PairsNum);        
        
        %% linear fit from the orginal CFD paper (Nikulin et al, 2012)
        batchout(sprintf('Linear least-squares optimization from CFD''2012''...'));
        [filter_q_vadim] = cfd2012_fit ( gcfd_elec_q, real(r) );
        batchout(sprintf('Done.'));
        
        for idx = 1:PairsNum
            pattern_q_vadim(1:gcfd_elec_q_num,idx) = filter2pattern(filter_q_vadim(:,idx), gcfd_elec_q);
        end
        
        %% Prepare the seeds for the nonlinear optimization
        if ~randomize_filter_seeds
            if filter_seed_cfd2012
                f0_default = filter_q_vadim;
            else
                f0_default = ones(gcfd_elec_q_num,PairsNum)/SensorsNum;
            end
            filter_seeds_num = 1;   % no need to have more than one same seed
        end
        
        %% Nonlinear optimization
        [filter_q] = gcfd_optimize ( m, r(:,1:PairsNum), FrP, f0_default, randomize_filter_seeds, filter_seeds_num, lsqnonlin_param );
        
%         if ssd_secondary_on
%             %% convert the filter in SSD coordinates to the filter in original coordinates
%             filter_q(ssd_secondary_comp_num+1:size(X_ssd_q,2),:) = 0;
%             filter_q = W_q * filter_q;
%         end

        clear pattern_q_gcfd
        for idx = 1:PairsNum
            pattern_q_gcfd(1:gcfd_elec_q_num,idx) = filter2pattern (filter_q(:,idx), gcfd_elec_q);
        end

        if ssd_secondary_on
            %% convert the pattern in SSD coordinates to the pattern in original coordinates
            pattern_q_gcfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
            pattern_q = pattern_q_ssd * pattern_q_gcfd;
        else
            pattern_q = pattern_q_gcfd;
        end
        
        %% Display results
        if FwdModel == FWD_MODEL_DATA
            [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
            fig.Color = 'white';
            colormap('jet');
            fig.Name = sprintf('Fig. %d: Recovered %dHz sources (same scale across rows)', fig_idx-1, FrQ*FrBase);
            
            rangescalp_p_abs = max(max(abs(pattern_p(:,1:PairsNum))));
            rangescalp_p = [-rangescalp_p_abs rangescalp_p_abs];
            
            rangescalp_q_cfd_abs = max(max(abs(pattern_q_vadim(:,1:PairsNum))));
            rangescalp_q_cfd = [-rangescalp_q_cfd_abs rangescalp_q_cfd_abs];
            
            rangescalp_q_abs = max(max(abs(pattern_q(:,1:PairsNum))));
            rangescalp_q = [-rangescalp_q_abs rangescalp_q_abs];
            
            for n=1:PairsNum
                subplot(3,PairsNum,n);   % Top: SSD recovered alpha sources
                ppp = pattern_p(:,n);
                %rangescalp = [ min(ppp) max(ppp)];
                plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', rangescalp_p, 'Shading', 'interp', 'Extrapolation', 1);
                if ssd_primary_on
                    title(sprintf('SSD %dHz #%d', FrP*FrBase, n));
                else
                    title(sprintf('True %dHz #%d', FrP*FrBase, n));
                end
                
                subplot(3,PairsNum,n+PairsNum);   % Middle: CFD'2012 recovered beta sources (just for reference)
                qqq = pattern_q_vadim(:,n);
                %rangescalp = [ min(qqq) max(qqq)];
                plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', rangescalp_q_cfd, 'Shading', 'interp', 'Extrapolation', 1);
                title(sprintf('CFD''2012 %dHz #%d', FrQ*FrBase, n));
                
                if calc_sync_measures
                    sig = elec_q * filter_q(:,n);    % the de-mixed component
                    kappa2 = kuramoto ( X_ssd_p(:,n), sig, FrP, FrQ );  % sync measure between the SSD component at FrP and the de-mixed component at FrQ
                    str_sync_measures = sprintf('\n%s = %.4f', '\kappa_{p-q}', kappa2);
                else
                    str_sync_measures = '';
                end
                
                subplot(3,PairsNum,n+2*PairsNum);   % Bottom: gCFD recovered beta sources
                qqq = pattern_q(:,n);
                %rangescalp = [ min(qqq) max(qqq)];
                plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', rangescalp_q, 'Shading', 'interp', 'Extrapolation', 1);
                title(sprintf('gCFD %dHz #%d%s', FrQ*FrBase, n, str_sync_measures));
            end
            
        end
        
        if (FwdModel == FWD_MODEL_GUIDO_MNI64) || (FwdModel == FWD_MODEL_RAND)
            
            [ pat_sig_q_idx, pat_q_idx, PRE_final(TIDX,:) ] = match_patterns_Volk2 ( pattern_sig_q, pattern_q, PairsNum );
            
            batchout(sprintf('gCFD pattern recovery error:'));
            disp(PRE_final(TIDX,:));
            
        end
        
        if FwdModel == FWD_MODEL_GUIDO_MNI64
            
            if source_topo_figures
                %% plotting recovered FrP sources
                [fig_handles, fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
                fig.Color = 'white';
                colormap('jet');
                fig.Name = sprintf('Fig. %d: Recovered %dHz sources', fig_idx-1, FrQ*FrBase);
                
                for n=1:PairsNum
                    subplot(3,PairsNum,n);   % Top: true beta sources
                    title(sprintf('True %dHz #%d', FrQ*FrBase, pat_sig_q_idx(n)));
                    showfield(pattern_sig_q(:,pat_sig_q_idx(n)), source_analysis.locs_2D);
                    
                    if ssd_secondary_on
                        iperm = invperm(pat_sig_q_idx);
                        subplot(3,PairsNum,iperm(pat_sig_q_ssd_idx(n))+PairsNum);   % Middle: SSD FrQ recovered beta sources (just for reference)   pat_sig_q_ssd_idx
                        
                        if calc_sync_measures
                            sig = gcfd_elec_q * filter_q(:,pat_q_ssd_idx(n));    % the de-mixed component
                            kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_ssd_idx(n)), sig, 1, 1 );  % sync measure between the true signal and the de-mixed (by SSD q) component
                            kappa2 = kuramoto ( X_ssd_p(:,pat_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD p component at FrP and the de-mixed (by SSD q) component at FrQ
                            str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f\n(maybe wrong..)', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
                        else
                            str_sync_measures = '';
                        end
                        
                        title(sprintf('SSD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_ssd_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_ssd_idx(n)), pattern_q_ssd(:,pat_q_ssd_idx(n))), str_sync_measures));
                        showfield(pattern_q_ssd(:,pat_q_ssd_idx(n)), source_analysis.locs_2D);
                    else
                        subplot(3,PairsNum,n+PairsNum);   % Middle: CFD'2012 recovered beta sources (just for reference)
                        title(sprintf('CFD''2012 %dHz #%d\nError = %.4f', FrQ*FrBase, pat_q_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_idx(n)), pattern_q_vadim(:,pat_q_idx(n)))));
                        showfield(pattern_q_vadim(:,pat_q_idx(n)), source_analysis.locs_2D);
                    end
                    
                    subplot(3,PairsNum,n+2*PairsNum);  % Bottom: gCFD recovered beta sources
                    
                    if calc_sync_measures
                        sig = gcfd_elec_q * filter_q(:,pat_q_idx(n));    % the de-mixed component
                        kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_idx(n)), sig, 1, 1 );  % sync measure between the true signal and the de-mixed component
                        kappa2 = kuramoto ( X_ssd_p(:,pat_q_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD component at FrP and the de-mixed component at FrQ
                        str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
                    else
                        str_sync_measures = '';
                    end

                    title(sprintf('gCFD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_idx(n),  PRE_final(TIDX,n), str_sync_measures));
                    showfield(pattern_q(:,pat_q_idx(n)), source_analysis.locs_2D);
                end
            end
            
            %                 %%%%%%%%
            %
            %                 [ mn2b, Rob, Cob ] = match_patterns_Volk ( pattern_sig(:,PairsNum+1:SourceNum), pattern_q, PairsNum );
            %
            %                 if source_topo_figures
            %                     fig = figure(2); clf % Plotting recovered beta sources
            %                     fig.Color = 'white';
            %                     colormap('jet');
            %                     fig.Name = sprintf('Recovered %dHz sources', FrQ*FrBase);
            %
            %                     for n=1:PairsNum
            %                         subplot(2,PairsNum,n);
            %                         title(sprintf('True %dHz #%d', FrQ*FrBase, Cob(n)));
            %                         showfield(pattern_sig(:,Cob(n)+PairsNum), source_analysis.locs_2D);   % Top: true beta sources
            %
            %                         subplot(2,PairsNum,n+PairsNum);
            %                         title(sprintf('gCFD %dHz #%d\nError = %.4f', FrQ*FrBase, Ro(n), mn2(n)));
            %                         title(sprintf('gCFD %dHz #%d', FrQ*FrBase, Rob(n)));
            %                         showfield(pattern_q(:,Rob(n)), source_analysis.locs_2D);        % Bottom: Denis's recovered beta sources
            %                     end
            %
            %                 end
            %
            %                 for n=1:PairsNum
            %                     patt_true = pattern_sig(:,Cob(n)+PairsNum);
            %                     patt_estimate = pattern_q(:,Rob(n));
            %                     PRE_final(TIDX,n) = vec_mismatch(patt_true,patt_estimate);
            %                 end
            %                 batchout(sprintf('Pattern recovery error:'));
            %                 disp(PRE_final(TIDX,:));
        end
        
        
        batchout('Batch completed.');
        
    end
    
    rng_saved_states = rng_states;
    save([output_dir sprintf('[P=%d FrP=%d FrQ=%d] rng_saved_states',PIDX,FrP,FrQ)], 'rng_saved_states');
    clearvars 'rng_saved_states';
    
    switch FwdModel
        case {FWD_MODEL_GUIDO_MNI64, FWD_MODEL_RAND}
            %%
            thresh = 0.20;
            pre_good_percent(FrP,FrQ) = floor(sum(PRE_final(:) < thresh) / numel(PRE_final) * 100);
            
            pre_mean = mean(PRE_final(:));
            pre_median = median(PRE_final(:));
            
            filename = sprintf('[P=%d FrP=%d FrQ=%d] pattern_recovery_error hist',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off'); 
            histogram(PRE_final, 0:0.025:1); grid on;
            legends{1} = (sprintf('gCFD error; #(PRE < %.2f) = %d (%d%%)', thresh, sum(PRE_final(:) < thresh), pre_good_percent(FrP,FrQ)));
            title(sprintf('PRE median = %.4f',pre_median));
            
            if ssd_primary_on && plot_ssd_error_hist
                hold on;
                histogram(PRE_ssd, 0:0.025:1,'FaceColor',[0.9 1 0.9],'FaceAlpha',0.3);
                legends{2} = sprintf('SSD error');
            end
            
            legend(legends);
            %columnlegend(2,legends);
            savefig([output_dir filename]);
            
            %% Composite PRE figure w subplots
            figure(fh_pre);
            subplot(Comp_PRE_Fig_Rows, Comp_PRE_Fig_Cols, pidx);
            histogram(PRE_final, 0:0.025:1); grid on;
            title(sprintf('p:q = %d:%d\nPRE median = %.4f',FrP,FrQ,pre_median));
            legend(sprintf('#(PRE < %.2f) = %d (%d%%)', thresh, sum(PRE_final(:) < thresh), pre_good_percent(FrP,FrQ)));           
            % will be saved to file later
            
            %%
            if ssd_primary_on && plot_dual_pre_ssd_error_hist
                filename = sprintf('[P=%d FrP=%d FrQ=%d] SSD err vs PRE',PIDX,FrP,FrQ);
                figure('name',filename,'NumberTitle','off');
                plot(PRE_ssd,PRE_final,'*');
                grid on;
                xlabel('SSD error'); ylabel('Final error');
                lls = {'1','2','3','4','5','6','7','8','9','10'};
                legend(lls{1:5});
                savefig([output_dir filename]);
            end
            
            %FontSmoothing set to 'off'
            
            %%
            filename = sprintf('[P=%d FrP=%d FrQ=%d] pattern_recovery_error',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off');
            hold on;
            if ~plot_trials_as_lines
                plot(PRE_final(:),'*');
                xlabel('Trials, Refs');
            else
                for TIDX = 1:batch_size
                    line([TIDX TIDX],[min(PRE_final(TIDX,:)), max(PRE_final(TIDX,:))]);
                end
                
                for k = 1:PairsNum
                    plot(1:batch_size,PRE_final(:,k),'b*','DisplayName',['Pair ' num2str(k)]);
                end
                
                xlabel('Trials');
            end
            grid on; ylabel('Pattern recovery error');
            hold off;
            savefig([output_dir filename]);
            
    end
    
    %% Save the composite PRE figure w subplots
    figure(fh_pre);
    savefig([output_dir fn_pre]);
    
    %%
    pidx = pidx+1;
end

switch FwdModel
    case {FWD_MODEL_GUIDO_MNI64, FWD_MODEL_RAND}
        if plot_pre_as_heatmap
            filename = sprintf('Percent of good recoveries');
            figure('name',filename,'NumberTitle','off');
            im = imagesc(pre_good_percent); 
            hold on; 
            ax = gca; ax.XTick = 1:max(FrQ); ax.YTick = 1:max(FrP);
            xlabel('q'); ylabel('p');
            colorbar; caxis([0 100]); colormap('hot');
            hold off;
            savefig([output_dir filename]);
        end
end
