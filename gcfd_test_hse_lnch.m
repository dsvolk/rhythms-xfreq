%% Generalized Cross-Frequency Decomposition (realistic, MNI-based) Head Simulation script
%% Denis VOLK, Centre for Cognition & Decision Making, National Research University Higher School of Economics, Moscow, Russia

warning('on','MATLAB:RandStream:ActivatingLegacyGenerators')
warning('on','MATLAB:RandStream:ReadingInactiveLegacyGeneratorState') 

%% Setup the environment
warning('off','MATLAB:dispatcher:pathWarning'); % suppress warnings about non-existent folders

addpath(genpath('.'));         % add all same-level-directories to MATLAB search path

addpath('/Users/1/MATLAB_Toolboxes/Guido_toolbox/');    % for Volkobook
%addpath('/Users/1/MATLAB_Toolboxes/bbci_public/');     % for Volkobook
addpath('C:/MATLAB_Toolboxes/Guido_toolbox/');          % for Denis's Windows machines
%addpath('C:/MATLAB_Toolboxes/bbci_public/');           % for Denis's Windows machines
addpath('../local/');                                   % for all Denis's machines

% Sasha, Daniil, please add your path strings here
addpath('/home/sasha/Desktop/cognition/Task2/BCI_tools');

warning('on','MATLAB:dispatcher:pathWarning');

%startup_bbci_toolbox;

%%
output_dir = ['./out_test_hse/' datestr(now,'yymmdd_HHMMSS') '/'];
mkdir(output_dir);

%% Preparing to start the script
diary([output_dir 'diary.txt']);
close all;
clearvars -except 'rng_saved_states' 'output_dir';
disp('================================================================================');
disp('==== gCFD simulation test=======================================================');
disp('================================================================================');
global PIDX;    % iterator over various parameter settings
global TIDX;    % iterator over trials within each PIDX
rng('shuffle','twister');
rng_seed_default = rng();

stdout([datestr(now,'dd/mm/yyyy') '. Ready to rock now!']);
disp('[HSE_Volk simulation launcher]');
ScriptCPUStart = cputime;

%% Set up the parameters
mk_clabs;
gcfd_test_hse_ctrl;

%% Main script
gcfd_test_batch;

%% Finishing the script
ScriptCPUFinish = cputime;
stdout(['Finished, taking ' sprintf('%.2f',ScriptCPUFinish-ScriptCPUStart) ' secs of CPU time.']);
diary off;

%% Play a sound
if sound_on_finish
    [snd,Fs] = audioread('batch_script_complete.m4a');
    sound(snd,Fs);
end

email_denis(['[MATLAB] HSE_Volk simulation has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.'],['The script "gcfd_head_sim.m" has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.']);
return;

