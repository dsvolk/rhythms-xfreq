function [filter_p, filter_q] = gcfd_duo_optimize ( mp, mq, FrP, FrQ, randomize_filter_seeds, filter_seeds_num, lsqnonlin_param )
% Core function for Dual-band Generalized Cross-Frequency Decomposition
%
% INPUT:
%     mp,mq - signals at frequency FrP, FrQ, complexified, matrixes TxM
%     FrP, FrQ - integer number
%
%     f0_default, randomize_filter_seeds, filter_seeds_num - settings for seeds for iterative nonlinear optimization
%     lsqnonlin_param - parameters for "lsqnonlin" function (MATLAB built-in)
% OUTPUT:
%     filter_q - spatial filters (demixing coefficients) for frequency FrQ, matrix MxN

%% so far, we assume size(mp,2) == size(mq,2)
SensorsNum = size(mp,2);

%% normalize sources
norm_p = sqrt(var(mp,0,1));
norm_p
mp = bsxfun(@rdivide, mp, norm_p);

norm_q = sqrt(var(mq,0,1));
norm_q
mq = bsxfun(@rdivide, mq, norm_q);

%% nonlinear least-squares optimization
%% define the function for 'lsqnonlin'
fun = @(f) abs((mp*f(1:SensorsNum)).^FrQ - (mq*f(SensorsNum+1:end)).^FrP); % default

%% assemle the options structure for the algorithm
lsqnonlin_param.MaxIter = 1;
oo_names = fieldnames(lsqnonlin_param);
oo_values = struct2cell(lsqnonlin_param);
ooo(1:2:2*length(oo_names)-1) = oo_names;
ooo(2:2:2*length(oo_names)) = oo_values;
options = optimoptions('lsqnonlin', ooo{:});

%% set up the seeds and call 'lsqnonlin'
if randomize_filter_seeds
    batchout(sprintf('[Seed Mode] randomize_filter_seeds with %d seeds:', filter_seeds_num));
    
    filter_tmp = NaN(2*SensorsNum, filter_seeds_num);
    resnorm = NaN(filter_seeds_num,1);
    
    for seed_idx = 1:filter_seeds_num
        batchout(sprintf('Random seed %d/%d:', seed_idx, filter_seeds_num));
        
        filter_tmp(:,seed_idx) = randn(2*SensorsNum,1);
        
        for k=1:100
            fprintf('.');
            filter_tmp(:,seed_idx) = filter_tmp(:,seed_idx) / sqrt(sum(filter_tmp(:,seed_idx).^2));
            [filter_tmp(:,seed_idx),resnorm(seed_idx),~,~,~] = lsqnonlin(fun,filter_tmp(:,seed_idx),[],[],options);
        end
        fprintf('\n');
    end
    
    [M,I] = min(resnorm(:));
    I
    
    if filter_seeds_num > 1
        stdout(sprintf('Residual norms for %d seed filters:', filter_seeds_num));
        disp(resnorm(:));
        stdout(sprintf('Resnorms std/mean = %.4f\n', std(resnorm(:)) / mean(resnorm(:))));
    end
    
    filter_p = filter_tmp(1:SensorsNum,I(1)) .* norm_p';
    filter_q = filter_tmp(SensorsNum+1:end,I(1)) .* norm_q';
    
else
    assert(false);
    batchout(sprintf('[Seed Mode] default.\n'));
    f0 = f0_default;
    filter_q = lsqnonlin(fun,f0,[],[],options);
end
end
