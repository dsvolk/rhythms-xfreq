%% Display results
if FwdModel == FwdModelType.Data
    [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
    fig.Color = 'white';
    colormap('jet');
    
    if source_topo_figures_normalize_ranges_across_scalps
        fig.Name = sprintf('Fig. %d: Recovered %dHz sources (same scale across rows)', fig_idx, FrQ*FrBase);
    else
        fig.Name = sprintf('Fig. %d: Recovered %dHz sources', fig_idx, FrQ*FrBase);
    end
    
    rangescalp_p_abs = max(max(abs(pattern_p(:,1:PairsNum))));
    rangescalp_p = [-rangescalp_p_abs rangescalp_p_abs];
    
    rangescalp_q_cfd_abs = max(max(abs(pattern_q_vadim(:,1:PairsNum))));
    rangescalp_q_cfd = [-rangescalp_q_cfd_abs rangescalp_q_cfd_abs];
    
    rangescalp_q_abs = max(max(abs(pattern_q(:,1:PairsNum))));
    rangescalp_q = [-rangescalp_q_abs rangescalp_q_abs];
    
    %%
    for n=1:PairsNum
        %subplot(3,PairsNum,n);   % Top: SSD recovered alpha sources
        subplot(3,PairsNum,n);
        ppp = pattern_p(:,n);
        if source_topo_figures_normalize_ranges_across_scalps
            ppp_range = rangescalp_p;
        else
            ppp_range = [-max(abs(ppp(:))), max(abs(ppp(:)))];
        end
        
        %%
        plot_scalp(GoodMnt2D, ppp, 'ShowLabels', 0, 'CLim', ppp_range, 'Shading', 'interp', 'Extrapolation', 1);
        %%
        if ssd_primary_on
            title(sprintf('SSD %dHz #%d', FrP*FrBase, n));
        else
            title(sprintf('True %dHz #%d', FrP*FrBase, n));
        end
        
        %     subplot(3,PairsNum,n+PairsNum);   % Middle: CFD'2012 recovered beta sources (just for reference)
        %     qqq = pattern_q_vadim(:,n);
        %     plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', rangescalp_q_cfd, 'Shading', 'interp', 'Extrapolation', 1);
        %     title(sprintf('CFD''2012 %dHz #%d', FrQ*FrBase, n));
        
        
        %subplot(3,PairsNum,n+2*PairsNum);   % Bottom: gCFD recovered beta sources
        subplot(3,PairsNum,n+PairsNum);
        qqq = pattern_q(:,n);
        if source_topo_figures_normalize_ranges_across_scalps
            qqq_range = rangescalp_q;
        else
            qqq_range = [-max(abs(qqq(:))), max(abs(qqq(:)))];
        end
        %%
        plot_scalp(GoodMnt2D, qqq, 'ShowLabels', 0, 'CLim', qqq_range, 'Shading', 'interp', 'Extrapolation', 1);
        %%
        if calc_sync_measures
            sig_q(:,n) = gcfd_elec_q * filter_q(:,n);    % the de-mixed component
            kappa2 = kuramoto ( X_ssd_p(:,n), sig_q(:,n), FrP, FrQ );  % sync measure between the SSD component at FrP and the de-mixed component at FrQ
            
            pat_diff = vec_mismatch (ppp,qqq);
            
            str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{p-q}', kappa2, '\angle', pat_diff);
        else
            str_sync_measures = '';
        end
        
        %%
        title(sprintf('gCFD %dHz #%d%s', FrQ*FrBase, n, str_sync_measures));
        
        
        subplot(3,PairsNum,n+2*PairsNum);   % Bottom: spectrums
        
        %%
        %figure;
        hold on; grid on;
        
        %limit_hertz = FrBase * (max(FrP,FrQ) + 1);
        
        [pxx,f] = pwelch(component_prim_ssd(:,n),Discr,[],[],Discr);
        
        fr_mask = (fr_cutoff_low < f) & (f < fr_cutoff_high);
        %fr_mask([1 2]) = 0;
        
        pxx = pxx / sum(pxx(fr_mask));
        %[pxx,f] = pwelch(component_p, 10*Discr, [], 2048, Discr);
        plot(f,10*log10(pxx),'LineWidth',2);
        %plot(f,pxx,'LineWidth',2);
        
        [pxx,f] = pwelch(component_gcfd(:,n),Discr,[],[],Discr);
        pxx = pxx / sum(pxx(fr_mask));
        %[pxx,f] = pwelch(component_q, 10*Discr, [], 2048, Discr);
        plot(f,10*log10(pxx),'LineWidth',2);
        %plot(f,pxx,'LineWidth',2);
        
        legend('SSD', 'gCFD');
        xlim([fr_cutoff_low fr_cutoff_high]);
        
        plot([FrBase*FrP FrBase*FrP], ylim, '--k');
        plot([FrBase*FrQ FrBase*FrQ], ylim, '--k');
        
        xlabel('Frequency (Hz)');
        ylabel('Magnitude (dB)');
        
        hold off;
    end
end