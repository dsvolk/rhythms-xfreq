%% Generalized Cross-Frequency Decomposition Duo (realistic, MNI-based) Head Simulation script
%% Denis VOLK, Centre for Cognition & Decision Making, National Research University Higher School of Economics, Moscow, Russia

warning('on','MATLAB:RandStream:ActivatingLegacyGenerators')
warning('on','MATLAB:RandStream:ReadingInactiveLegacyGeneratorState') 

%% Setup the environment
warning('off','MATLAB:dispatcher:pathWarning'); % suppress warnings about non-existent folders

addpath(genpath('..'));         % add all same-level-directories to MATLAB search path

addpath('/Users/1/MATLAB_Toolboxes/Guido_toolbox/');    % for Volkobook
%addpath('/Users/1/MATLAB_Toolboxes/bbci_public/');      % Berlin BCI for Volkobook

addpath('C:/MATLAB_Toolboxes/Guido_toolbox/');   % for Denis's Windows machines
%addpath('C:/MATLAB_Toolboxes/bbci_public/');     % Berlin BCI for Denis's Windows machines

% Sasha, Daniil, please add your path strings here

warning('on','MATLAB:dispatcher:pathWarning');

%startup_bbci_toolbox;

output_dir = ['./duo_out_test_dev/' datestr(now,'yymmdd_HHMMSS') '/'];
mkdir(output_dir);

%% Preparing to start the script
diary([output_dir 'diary.txt']);
close all;
clearvars -except 'rng_saved_states' 'output_dir';
disp('================================================================================');
disp('==== gCFD Duo simulation test===================================================');
disp('================================================================================');
global PIDX;    % iterator over various parameter settings
global TIDX;    % iterator over trials within each PIDX
rng('shuffle','twister');
rng_seed_default = rng();

stdout([datestr(now,'dd/mm/yyyy') '. Ready to rock now!']);
disp('[Developer/debug simulation launcher]');
ScriptCPUStart = cputime;

%% Set up the parameters
mk_clabs;
gcfd_duo_test_dev_ctrl;

%% Main script
gcfd_duo_test_batch;

%% Finishing the script
ScriptCPUFinish = cputime;
stdout(['Finished, taking ' sprintf('%.2f',ScriptCPUFinish-ScriptCPUStart) ' secs of CPU time.']);
diary off;

%% Play a sound
if sound_on_finish
    [snd,Fs] = audioread('batch_script_complete.m4a');
    sound(snd,Fs);
end

% email_denis(['[MATLAB] Developer/debug simulation has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.'],['The script "gcfd_head_sim.m" has finished on ' datestr(now,'dd/mm/yyyy') ' at ' datestr(now, 'HH:MM:SS') '.']);
return;

