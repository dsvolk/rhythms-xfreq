function [PRE_1_ssd_primary, PRE_1_ssd_secondary, PRE_lin, PRE_1_final, fig_idx] = gcfd_test ...
    ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
    butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a, ...
    source_analysis, GoodMnt2D, param, ...
    fig_handles, fig_idx, output_dir, plotting_on )

unpack_param(param, 'param');

assert ((FwdModel == FwdModelType.Rand) || (FwdModel == FwdModelType.GuidoMNI), 'Only ''FwdModelType.Rand'' and ''FwdModelType.GuidoMNI'' are supported.\nUse ''gcfd_analyze.m'' for what used to be ''FwdModelType.Data''');

%% Main part
if ssd_primary_on
    batchout('SSD for FrP components...');
    %% run SSD to recover FrP sources
    [W, pattern_p, lambda, C_s, X_ssd_p] = ssd(elec, [FrP*FrBase-1 FrP*FrBase+1; FrP*FrBase-3 FrP*FrBase+3; FrP*FrBase-2 FrP*FrBase+2], Discr, 2, [1 T]);
    batchout('Done.');
    
    %% save the unfiltered components
    component_prim_ssd = elec * W(:,1:PairsNum);
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrP*FrBase, lambda, PairsNum, ssd_secondary_comp_num );
        end
    end
    
    %% match_patterns
    [ pat_sig_p_idx, pat_p_idx, PRE_1_ssd_primary ] = match_patterns_Volk2 ...
        ( pattern_sig_p, pattern_p(:,1:PairsNum), PairsNum );
    batchout(sprintf('SSD for FrP pattern recovery error:'));
    disp(PRE_1_ssd_primary);
    
    %% true patterns vs ssd patterns
    if FwdModel == FwdModelType.GuidoMNI
        if plotting_on && source_topo_figures
            [fig_idx] = plot_topo_true_vs_ssd_func ( fig_handles, fig_idx, '(primary)', output_dir, FrP*FrBase, lambda, ...
                PairsNum, 3, pattern_sig_p, pat_sig_p_idx, src_sig_p, pattern_p, pat_p_idx, X_ssd_p, ...
                PRE_1_ssd_primary, source_analysis );
        end
    end
    
else    
    batchout('SSD is off. Instead, will be using the ''true'' FrP patterns and ''true'' signals for gCFD.');
    pattern_p = pattern_sig_p;
    X_ssd_p = src_sig_p;
    PRE_1_ssd_primary = NaN;
end

PRE_1_ssd_secondary = NaN;
if ssd_secondary_on
    batchout('SSD for FrQ components...');
    %% run SSD to recover FrQ sources
    [W_q, pattern_q_ssd, lambda, C_s, X_ssd_q] = ssd(elec, [FrQ*FrBase-1 FrQ*FrBase+1; FrQ*FrBase-3 FrQ*FrBase+3; FrQ*FrBase-2 FrQ*FrBase+2], Discr, 2, [1 T]);
    batchout('Done.');
    
    %% plots
    if plotting_on
        if plot_ssd_lambda
            [fig_idx] = plot_ssd_lambda_func ( fig_handles, fig_idx, output_dir, FrQ*FrBase, lambda, PairsNum, ssd_secondary_comp_num );
        end
    end
    
    %% match_patterns
    [ pat_sig_q_ssd_idx, pat_q_ssd_idx, PRE_1_ssd_secondary ] = match_patterns_Volk2 ...
        ( pattern_sig_q, pattern_q_ssd(:,1:PairsNum), PairsNum );
    batchout(sprintf('SSD for FrQ pattern recovery error:'));
    disp(PRE_1_ssd_secondary);
    
    %% true patterns vs ssd patterns
    if FwdModel == FwdModelType.GuidoMNI
        if plotting_on && source_topo_figures
            [fig_idx] = plot_topo_true_vs_ssd_func ( fig_handles, fig_idx, '(secondary)', output_dir, FrQ*FrBase, lambda, ...
                PairsNum, 3, pattern_sig_q, pat_sig_q_ssd_idx, src_sig_q, pattern_q_ssd, pat_q_ssd_idx, X_ssd_q, ...
                PRE_1_ssd_secondary, source_analysis );
        end
    end
else
    batchout('SSD for FrQ is off. Running gCFD on raw channels.');
    PRE_1_ssd_secondary = NaN;
end

batchout('gCFD...');
%% genCFD DECOMPOSITION PART:
%% prepare the signals
gcfd_elec_q_num = NaN;
if ssd_secondary_on
    gcfd_elec_q_num = min([size(X_ssd_q,2) ssd_secondary_comp_num]);
    gcfd_elec_q = X_ssd_q(:,1:gcfd_elec_q_num);
else
    gcfd_elec_q_num = size(elec,2);
    gcfd_elec_q = filtfilt(butter_q_b,butter_q_a,elec);
end

gcfd_elec_q_num
filter_q = NaN(gcfd_elec_q_num,PairsNum);

gcfd_elec_q = detrend(gcfd_elec_q,'constant');
X_ssd_p = detrend(X_ssd_p,'constant');

%% complexify the signals (compute 'analytic signals')
m = hilbert(gcfd_elec_q);
r = hilbert(X_ssd_p);

%% frequency warp the SSD component
if targets_abs_1
    r = exp(1i.*(FrQ*angle(r)));
else
    r = abs(r).*exp(1i.*(FrQ*angle(r)));
end

%% filter the warped components
if post_warp_pq_filter_on
    r = hilbert(filtfilt(butter_pq_b,butter_pq_a,real(r)));
end

%% Linear fit from the orginal paper (Nikulin et al, "Cross-Frequency Decomposition...", 2012)
if FrP == 1
    batchout(sprintf('Linear least-squares optimization from CFD''2012''...'));
    [filter_q_vadim] = cfd2012_fit ( gcfd_elec_q, real(r) );
    batchout(sprintf('Done.'));

    %% Filter-to-pattern
    for idx = 1:PairsNum
        pattern_q_vadim_cfd(1:gcfd_elec_q_num,idx) = filter2pattern(filter_q_vadim(:,idx), gcfd_elec_q);
    end
end

%% Prepare the seeds for the nonlinear optimization
f0_default = ones(gcfd_elec_q_num,PairsNum)/gcfd_elec_q_num;

if filter_seed_cfd2012
    assert(FrP == 1);
    f0_default = filter_q_vadim;
end

if filter_seed_ssd
    assert (ssd_primary_on, 'filter_seed_ssd == true is not compatible with ssd_primary_on == false');
    assert (~ssd_secondary_on, 'filter_seed_ssd == true is not compatible with ssd_secondary_on == true');
    
    elec_p = filtfilt(butter_p_b,butter_p_a,elec);
    cov_p = elec_p' * elec_p;
    elec_q = filtfilt(butter_q_b,butter_q_a,elec);
    cov_q = elec_q' * elec_q;
    
    f0_default = cov_q \ cov_p * W(:,1:PairsNum);
end

if ~randomize_filter_seeds
    filter_seeds_num = 1;   % no need to have more than one same seed
end

assert (~norm_matrix_ord_on);   % just in case. because the commented code below is totally wrong.
%if norm_matrix_ord_on
%    %% normalize signals' orders of magnitude
%     assert(FrP == 1);
%     assert(1e-22 <= abs(mean(mean(m))) && abs(mean(mean(m))) <= 1e-19);
%     assert(1e-7  <= abs(mean(mean(r))) && abs(mean(mean(r))) <= 1e-5 );
% 
%     % Denis: should be mean(abs(m(:))) here ^^^
%     
%     D = 10^3;
%     m = m*D;
%     r = r/D;
% 
%     D = 10^13;
%     m = m*D;
%     r = r*D;
% 
%     assert(1e-6 <= abs(mean(mean(m))) && abs(mean(mean(m))) <= 1e-3);
%     assert(1e+3 <= abs(mean(mean(r))) && abs(mean(mean(r))) <= 1e+5);
%end

%% inject a head topo plot
if lsqnonlin_inject_topo_plot
    assert(~ssd_secondary_on, 'inject_head_topo_plot == true only works with ssd_secondary_on == false');
    lsqnonlin_param.PlotFcn{end+1} = @(x,optimValues,state,varargin) lsqnonlin_plot_topo(x,optimValues,state,varargin, gcfd_elec_q, GoodMnt2D);
end

%% Nonlinear optimization
if project_out_found_sources
    gcfd_elec_q_ortho = gcfd_elec_q;
    
    idx = 1;
    batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
    filter_q_ortho{idx} = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
    inv_transform_all{idx} = eye(gcfd_elec_q_num);
    
    for idx = 2:PairsNum
        [gcfd_elec_q_ortho, inv_transform_one_step{idx}] = project_to_ortho ( gcfd_elec_q_ortho, filter_q_ortho{idx-1} );
        batchout(sprintf('[%d/%d] Projected out the gCFD source for SSD component %d.', idx-1, PairsNum, idx-1));
        
        inv_transform_all{idx} = inv_transform_all{idx-1} * inv_transform_one_step{idx};
        m = hilbert(gcfd_elec_q_ortho);        
                
        batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
        filter_q_ortho{idx} = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
    end
    
    for idx = 1:PairsNum
        filter_q(:,idx) = inv_transform_all{idx} * filter_q_ortho{idx};
    end       
else
    for idx = 1:PairsNum
        %% for each of FrP sources...
        batchout(sprintf('[%d/%d] Nonlinear least-squares optimization for SSD component %d of %d...', idx, PairsNum, idx, PairsNum));
        filter_q(:,idx) = gcfd_optimize ( m, r(:,idx), FrP, f0_default(:,idx), randomize_filter_seeds, filter_seeds_num, lsqnonlin_param, sasha_optimization_tweak_on, sasha_f0_norm, sasha_step );
        batchout(sprintf('Filter %d:', idx));
        disp(filter_q(:,idx));
    end
end

%% Save the unfiltered components
if ssd_secondary_on    
    W_q_reduced = W_q (:,1:size(filter_q,1));
else    
    W_q_reduced = ones(size(filter_q,1));
end
component_gcfd = elec * W_q_reduced * filter_q;

%% Filter-to-pattern
clear pattern_q_gcfd
pattern_q_gcfd = NaN(gcfd_elec_q_num,PairsNum); 
for idx = 1:PairsNum
    pattern_q_gcfd(1:gcfd_elec_q_num,idx) = filter2pattern (filter_q(:,idx), gcfd_elec_q);
end

if ssd_secondary_on
    %% Convert the pattern in SSD coordinates to the pattern in original coordinates
    pattern_q_gcfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
    pattern_q = pattern_q_ssd * pattern_q_gcfd;
    
    if FrP == 1
    pattern_q_vadim_cfd (gcfd_elec_q_num+1:size(X_ssd_q,2),:) = 0;
    pattern_q_vadim = pattern_q_ssd * pattern_q_vadim_cfd;
    end
else
    pattern_q = pattern_q_gcfd;   
    if FrP == 1
        pattern_q_vadim = pattern_q_vadim_cfd;
    end
end

%%
[ pat_sig_q_idx, pat_q_idx, PRE_1_final ] = match_patterns_Volk2 ( pattern_sig_q, pattern_q, PairsNum );

batchout(sprintf('gCFD pattern recovery error:'));
disp(PRE_1_final);

if FrP == 1
    [ pat_sig_q_idx, pat_q_idx, PRE_lin ] = match_patterns_Volk2 ( pattern_sig_q, pattern_q_vadim, PairsNum );
    
    batchout(sprintf('gCFD pattern recovery error (lin):'));
    disp(PRE_lin);
else
    PRE_lin = 0;
end


if FwdModel == FwdModelType.GuidoMNI  
    if plotting_on && source_topo_figures
        %% plotting recovered FrP sources
        [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
        fig.Color = 'white';
        colormap('jet');
        fig.Name = sprintf('Fig. %d: Recovered %dHz sources', fig_idx, FrQ*FrBase);
        filename = sprintf('[Fig %d] Recovered %dHz sources', fig_idx, FrQ*FrBase);
        
%         if (ssd_secondary_on || (FrP == 1))
%             rowsNum = 3;
%         else
%             rowsNum = 2;
%         end
        rowsNum = 2;
        
        for n=1:PairsNum       
            %% Top: true beta sources
            subplot(rowsNum,PairsNum,n);   
            title(sprintf('True %dHz #%d', FrQ*FrBase, pat_sig_q_idx(n)));
            showfield(pattern_sig_q(:,pat_sig_q_idx(n)), source_analysis.locs_2D);
            
%             %% Middle: SSD FrQ recovered beta sources (just for reference)   pat_sig_q_ssd_idx
%             if ssd_secondary_on
%                 %iperm = invperm(pat_sig_q_idx);
%                 iperm = [1 2 3 4 5];
%                 subplot(rowsNum,PairsNum,iperm(pat_sig_q_ssd_idx(n))+PairsNum);   
%                 
%                 if calc_sync_measures
%                     sig = X_ssd_q(:,pat_q_ssd_idx(n));
%                     %sig = gcfd_elec_q * filter_q(:,pat_q_ssd_idx(n));    % the de-mixed component
%                     kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_ssd_idx(n)), sig, 1, 1 );  % sync measure between the true signal at FrQ and the de-mixed (by SSD q) component
%                     %kappa2 = kuramoto ( X_ssd_p(:,pat_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD p component at FrP and the de-mixed (by SSD q) component at FrQ
%                     kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the de-mixed (by SSD q) component at FrQ
%                     str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
%                 else
%                     str_sync_measures = '';
%                 end
%                 
%                 title(sprintf('SSD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_ssd_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_ssd_idx(n)), pattern_q_ssd(:,pat_q_ssd_idx(n))), str_sync_measures));
%                 
% %                 ---
% %                 
% %                 subplot(rowsNum,PairsNum,n+PairsNum);   % Middle: SSD FrQ recovered beta sources (just for reference)   pat_sig_q_ssd_idx
% %                 
% %                 if calc_sync_measures
% %                     sig = gcfd_elec_q * filter_q(:,pat_q_ssd_idx(n));    % the de-mixed component
% %                     kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_ssd_idx(n)), sig, 1, 1 );  % sync measure between the true signal at FrQ and the de-mixed (by SSD q) component
% %                     %kappa2 = kuramoto ( X_ssd_p(:,pat_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD p component at FrP and the de-mixed (by SSD q) component at FrQ
% %                     kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_ssd_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the de-mixed (by SSD q) component at FrQ
% %                     str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
% %                 else
% %                     str_sync_measures = '';
% %                 end
% %                 
% %                 title(sprintf('SSD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_ssd_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_ssd_idx(n)), pattern_q_ssd(:,pat_q_ssd_idx(n))), str_sync_measures));
%                 
%                 showfield(pattern_q_ssd(:,pat_q_ssd_idx(n)), source_analysis.locs_2D);
%             else
%                 if FrP == 1
%                     subplot(rowsNum,PairsNum,n+PairsNum);   % Middle: CFD'2012 recovered beta sources (just for reference)
%                     title(sprintf('CFD''2012 %dHz #%d\nError = %.4f', FrQ*FrBase, pat_q_idx(n), vec_mismatch(pattern_sig_q(:,pat_sig_q_idx(n)), pattern_q_vadim(:,pat_q_idx(n)))));
%                     showfield(pattern_q_vadim(:,pat_q_idx(n)), source_analysis.locs_2D);
%                 end
%             end
            
            %% Bottom: gCFD recovered beta sources
            subplot(rowsNum,PairsNum,n+(rowsNum-1)*PairsNum);  
            
            if calc_sync_measures
                sig = gcfd_elec_q * filter_q(:,pat_q_idx(n));    % the de-mixed component
                kappa1 = kuramoto ( src_sig_q(:,pat_sig_q_idx(n)), sig, 1, 1 );  % sync measure between the true signal and the gCFD de-mixed component at FrQ
                %kappa2 = kuramoto ( X_ssd_p(:,pat_q_idx(n)), sig, FrP, FrQ );  % sync measure between the SSD component at FrP and the gCFD de-mixed component at FrQ
                kappa2 = kuramoto ( src_sig_p(:,pat_sig_q_idx(n)), sig, FrP, FrQ );  % sync measure between the true signal at FrP and the gCFD de-mixed component at FrQ
                str_sync_measures = sprintf('\n%s = %.4f\n%s = %.4f', '\kappa_{vs true}', kappa1, '\kappa_{p-q}', kappa2);
            else
                str_sync_measures = '';
            end
            
            title(sprintf('gCFD %dHz #%d\nError = %.4f%s', FrQ*FrBase, pat_q_idx(n),  PRE_1_final(n), str_sync_measures));
            showfield(pattern_q(:,pat_q_idx(n)), source_analysis.locs_2D);
        end
        
        drawnow;
        savefig([output_dir filename]);
    end
end

%% these figures are temporary disabled - no time to debug
if false
    
    %% research component self-sync on different frequencies
    %% primary SSD components
    if ssd_primary_on
        component_prim_ssd_p = filtfilt(butter_p_b,butter_p_a,component_prim_ssd);
        component_prim_ssd_q = filtfilt(butter_q_b,butter_q_a,component_prim_ssd);
    end
    
    %% final recovered components
    component_gcfd_p = filtfilt(butter_p_b,butter_p_a,component_gcfd);
    component_gcfd_q = filtfilt(butter_q_b,butter_q_a,component_gcfd);
    
    
    %     [fig_idx, fig] = figure_reopen_clf ( fig_handles, fig_idx );
    %     fig.Color = 'white';
    %     colormap('jet');
    
    kappa_all = NaN(PairsNum+2, PairsNum+2);
    corr_all = NaN(PairsNum+2, PairsNum+2);
    
    for i=1:PairsNum
        for j=1:PairsNum
            kappa_all(i,j) = kuramoto ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ );
            corr_all(i,j) = sync_index_corr ( X_ssd_p(:,i), sig_q(:,j), FrP, FrQ, Discr/FrBase/2 );
        end
    end
    
    batchout('Primary SSD components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(i,end) = kuramoto ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ );
        corr_all(i,end) = sync_index_corr ( component_prim_ssd_p(:,i), component_prim_ssd_q(:,i), FrP, FrQ, Discr/FrBase/2 );
    end
    batchout('Final gCFD recovered components - self-sync on different frequencies');
    for i=1:PairsNum
        kappa_all(end,i) = kuramoto ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ );
        corr_all(end,i) = sync_index_corr ( component_gcfd_p(:,i), component_gcfd_q(:,i), FrP, FrQ, Discr/FrBase/2 );
    end
    
    %%
    filename = sprintf('Sync Indices');
    figure('name',filename,'NumberTitle','off');
    
    pcolor([kappa_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);
    
    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3;
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3;
    ax.YTickLabel = {'1','2','3','4','5','','harmo'};
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(kappa_all(:))]); colormap('summer');
    hold off;
    %savefig([output_dir filename]);
    
    %%
    filename = sprintf('Correlations');
    figure('name',filename,'NumberTitle','off');
    
    pcolor([corr_all NaN(PairsNum+2,1); NaN(1,PairsNum+3)]);
    
    hold on;
    ax = gca;
    ax.XTick = 1.5:1:PairsNum+3;
    ax.XTickLabel = {'1','2','3','4','5','','harmo'};
    ax.YTick = 1.5:1:PairsNum+3;
    ax.YTickLabel = {'1','2','3','4','5','','harmo'};
    
    xlabel('gCFD components'); ylabel('SSD components');
    colorbar; caxis([0 max(corr_all(:))]); colormap('autumn');
    hold off;
    %savefig([output_dir filename]);
    
    %%
    batchout('Cross- and self-sync on different frequencies');
    kappa_all
    batchout('Cross- and self-correlations on different frequencies');
    corr_all
end

%% Show the channel labels
%figure; plot_scalp(GoodMnt2D, zeros(size(GoodMnt2D.x)), 'ShowLabels', 1, 'CLim', [-1,1], 'Shading', 'interp', 'Extrapolation', 1);


