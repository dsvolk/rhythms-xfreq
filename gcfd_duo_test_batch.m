%% Load GoodChannels and source_analysis/montage data
[source_analysis, GoodMnt2D, GoodChannels] = load_chan_info (default);

%% Saving the percentages of good recoveries
pre_good_percent_pq_frp = NaN (max([param(:).FrP]), max([param(:).FrQ]));
pre_good_percent_pq_frq = NaN (max([param(:).FrP]), max([param(:).FrQ]));
pre_median_pq_frp = NaN (max([param(:).FrP]), max([param(:).FrQ]));
pre_median_pq_frq = NaN (max([param(:).FrP]), max([param(:).FrQ]));

%% Prepare the composite figure w subplots
if plotting_on
    fn_pre = sprintf('PRE - all params');
    fh_pre = figure('Name',fn_pre,'NumberTitle','off');
end

%% Launch the Random Number Generator
rng(rng_seed);

%% Parameters' for loop
pidx = 1;   % in case PARAMS is not from 1 to N, this is an iterator starting at 1
stdout(sprintf('Multi-batch mode with %d batches.', length(PARAMS)));
for PIDX = PARAMS    
    %% Copy param(PIDX) structure fields to local variables - for brevity of code
    unpack_param(param(PIDX), sprintf('param(%d)',PIDX));   
    assert ((FwdModel == FwdModelType.Rand) || (FwdModel == FwdModelType.GuidoMNI), 'Only ''FwdModelType.Rand'' and ''FwdModelType.GuidoMNI'' are supported. Use ''gcfd_analyze.m'' for what used to be ''FwdModelType.Data''');   
    
    %% Allocate some space for results
    PRE_1_ssd_primary = NaN(batch_size,PairsNum);
    PRE_1_ssd_secondary = NaN(batch_size,PairsNum);
    PRE_1_final = NaN(batch_size,PairsNum);
    
    PRE_2_ssd_primary = NaN(batch_size,PairsNum);
    PRE_2_ssd_secondary = NaN(batch_size,PairsNum);
    PRE_2_final = NaN(batch_size,PairsNum);
    
    %%
    clearvars 'rng_states';
    
    stdout(sprintf('Batch %d of %d started.', PIDX, length(PARAMS)));
    stdout('Param = ');
    disp(param(PIDX));
    stdout('lsqnonlin_param = ');
    disp(param(PIDX).lsqnonlin_param);
    
    %% create band-pass filters
    [butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a] = ...
        mk_bp_filters ( FrP, FrQ, FrBase, Discr );
    
    %% Main batch loop
    for TIDX = 1:batch_size
        
        fig_handles = gobjects(100,1);
        fig_idx = 0;
        
        rng_states(TIDX) = rng();
        
        batchout('Yiihaaa!.. another trial.');
                
        %% Make up or load the electrode recordings
        [elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q] = mk_elec (source_analysis, GoodMnt2D, param(PIDX), ...
            butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a );
        
    	%% Preprocess the raw data
    	[butter_preproc_b, butter_preproc_a] = butter(2,[fr_cutoff_low fr_cutoff_high]/(Discr/2));
    	elec = filtfilt (butter_preproc_b, butter_preproc_a, elec);

        %% Now we attempt to decompose them back
        [PRE_p_final, PRE_q_final, fig_idx] = gcfd_duo_test ...
            ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
            butter_p_b, butter_p_a, butter_q_b, butter_q_a, ...
            source_analysis, GoodMnt2D, param, ...
            fig_handles, fig_idx, output_dir, plotting_on );
        
        batchout('Batch completed.');
        
    end
    
    rng_saved_states = rng_states;
    save([output_dir sprintf('[P=%d FrP=%d FrQ=%d] rng_saved_states',PIDX,FrP,FrQ)], 'rng_saved_states');
    clearvars 'rng_saved_states';
    
    %% temporarily disabled
    if false      
        %%
        thresh = 0.20;
        pre_good_percent_pq(FrP,FrQ) = floor(sum(PRE_1_final(:) < thresh) / numel(PRE_1_final) * 100);
        
        pre_mean = mean(PRE_1_final(:));
        pre_median = median(PRE_1_final(:));
        pre_median_pq_frp(FrP,FrQ) = pre_median;
        
        if plot_pre_hist
            filename = sprintf('[P=%d FrP=%d FrQ=%d] pattern_recovery_error hist',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off');
            histogram(PRE_1_final, 0:freedman_diaconis(PRE_1_final):1); grid on;
            legends{1} = (sprintf('gCFD error; #(PRE < %.2f) = %d (%d%%)', thresh, sum(PRE_1_final(:) < thresh), pre_good_percent_pq(FrP,FrQ)));
            title(sprintf('PRE median = %.4f',pre_median));
            legend(legends);
            drawnow;
            savefig([output_dir filename]);
        end
        
        if plot_separate_hists_for_ssd_comps
            for ssd_idx = 1:PairsNum
                filename = sprintf('[P=%d FrP=%d FrQ=%d SSDq=%d] pattern_recovery_error hist',PIDX,FrP,FrQ,ssd_idx);
                figure('name',filename,'NumberTitle','off');
                histogram(PRE_1_final(:,ssd_idx), 0:freedman_diaconis(PRE_1_final(:,ssd_idx)):1); grid on;
                legends{1} = (sprintf('gCFD error; #(PRE < %.2f) = %d', thresh, sum(PRE_1_final(:,ssd_idx) < thresh)));
                title(sprintf('PRE median = %.4f',median(PRE_1_final(:,ssd_idx))));
                legend(legends);
                drawnow;
                savefig([output_dir filename]);
            end
        end
        
        if ssd_primary_on && plot_ssd_error_hist
            filename = sprintf('[P=%d FrP=%d FrQ=%d] SSD_error hist',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off');
            histogram(PRE_1_ssd_primary, 0:freedman_diaconis(PRE_1_ssd_primary):1); grid on;
            %histogram(PRE_1_ssd_primary, 0:freedman_diaconis(PRE_1_final):1,'FaceColor',[0.9 1 0.9],'FaceAlpha',0.3);
            legends{1} = (sprintf('SSD error; #(SSDE < %.2f) = %d', thresh, sum(PRE_1_ssd_primary(:) < thresh)));
            title(sprintf('SSDE median = %.4f',median(PRE_1_ssd_primary(:))));
            legend(legends);
            drawnow;
            savefig([output_dir filename]);
        end
        
        %% Composite PRE figure w subplots
        figure(fh_pre);
        subplot(Comp_PRE_Fig_Rows, Comp_PRE_Fig_Cols, pidx);        
        histogram(PRE_1_final, 0:freedman_diaconis(PRE_1_final):1); grid on;
        %title(sprintf('P=%d SNR=%.1g p:q=%d:%d\nPRE median = %.4f',PIDX,SNR,FrP,FrQ,pre_median));
        title(sprintf('P=%d Secs=%d p:q=%d:%d\nPRE median = %.4f',PIDX,Secs,FrP,FrQ,pre_median));
        legend(sprintf('#(PRE < %.2f) = %d (%d%%)', thresh, sum(PRE_1_final(:) < thresh), pre_good_percent_pq(FrP,FrQ)));
        % will be saved to file later
        
        %%
        if ssd_primary_on && plot_ssd_error_vs_pre
            filename = sprintf('[P=%d FrP=%d FrQ=%d] SSD err vs PRE',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off');
            plot(PRE_1_ssd_primary,PRE_1_final,'*');
            grid on;
            xlabel('SSD error'); ylabel('Final error');
            lls = {'1','2','3','4','5','6','7','8','9','10'};
            legend(lls{1:5});
            drawnow;
            savefig([output_dir filename]);
        end
        
        %FontSmoothing set to 'off'
        
        %%
        if plot_pre_trials
            filename = sprintf('[P=%d FrP=%d FrQ=%d] pattern_recovery_error',PIDX,FrP,FrQ);
            figure('name',filename,'NumberTitle','off');
            hold on;
            if ~plot_trials_as_lines
                plot(PRE_1_final(:),'*');
                xlabel('Trials, Refs');
            else
                for TIDX = 1:batch_size
                    line([TIDX TIDX],[min(PRE_1_final(TIDX,:)), max(PRE_1_final(TIDX,:))]);
                end
                
                for k = 1:PairsNum
                    plot(1:batch_size,PRE_1_final(:,k),'b*','DisplayName',['Pair ' num2str(k)]);
                end
                
                xlabel('Trials');
            end
            grid on; ylabel('Pattern recovery error');
            hold off;
            drawnow;
            savefig([output_dir filename]);
        end
        
        %% Save the composite PRE figure w subplots
        figure(fh_pre);
        drawnow;
        savefig([output_dir fn_pre]);
    end
    
    %%
    pidx = pidx+1;
end

%% temporarily disabled
if false
    %%
    if plot_pre_as_heatmap
        filename = sprintf('PRE_good_percent(p,q)');
        figure('name',filename,'NumberTitle','off');
        im = imagesc(pre_good_percent_pq);
        hold on;
        ax = gca; ax.XTick = 1:max([param(:).FrQ]); ax.YTick = 1:max([param(:).FrP]);
        xlabel('q'); ylabel('p');
        colorbar; caxis([0 100]); 
        cmap = brewermap([],'PuBu');
        colormap(flipud(cmap(1:56,:)));
        
        for a=ax.XTick
            for b=ax.YTick                
                if isnan(pre_good_percent_pq(a,b))
                    text(b,a,char(8211),'HorizontalAlignment','center','VerticalAlignment','middle');
                    rectangle('Position',[a-0.5, a-0.5, 1, 1]); %, 'FillPattern','DiagonalLines');
                else
                    text(b,a,sprintf('%.1f%%', pre_good_percent_pq(a,b)),'HorizontalAlignment','center','VerticalAlignment','middle');
                end
            end
        end
        
        hold off;
        drawnow;
        savefig([output_dir filename]);
    end
    %%
    if plot_pre_median_as_heatmap
        filename = sprintf('PRE_median(p,q)');
        figure('name',filename,'NumberTitle','off');
        im = imagesc(pre_median_pq_frp);
        hold on;
        ax = gca; ax.XTick = 1:max([param(:).FrQ]); ax.YTick = 1:max([param(:).FrP]);
        xlabel('q'); ylabel('p');
        colorbar; caxis([0 1]);
        cmap = brewermap([],'YlGn');
        colormap(cmap);
        
        for a=ax.XTick
            for b=ax.YTick
                if isnan(pre_median_pq_frp(a,b))
                    text(b,a,char(8211),'HorizontalAlignment','center','VerticalAlignment','middle');
                    rectangle('Position',[a-0.5, a-0.5, 1, 1]); %, 'FillPattern','DiagonalLines'); 
                else
                    text(b,a,sprintf('%.4f', pre_median_pq_frp(a,b)),'HorizontalAlignment','center','VerticalAlignment','middle');
                end
            end
        end
     
        hold off;
        drawnow;
        savefig([output_dir filename]);
    end
end
