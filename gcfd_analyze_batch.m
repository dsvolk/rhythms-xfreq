%% Load GoodChannels and source_analysis/montage data
%[source_analysis, GoodMnt2D, GoodChannels] = load_chan_info (default);

%% Launch the Random Number Generator
rng(rng_seed);

%% Parameters' for loop
pidx = 1;   % in case PARAMS is not from 1 to N, this is an iterator starting at 1
stdout(sprintf('Multi-batch mode with %d batches.', length(PARAMS)));
for PIDX = PARAMS    
    %% Copy param(PIDX) structure fields to local variables - for brevity of code
    unpack_param(param(PIDX), sprintf('param(%d)',PIDX));   
       
    fig_handles = gobjects(100,1);
    fig_idx = 0;
    
    clearvars 'rng_states';
    
    stdout(sprintf('Analysis %d of %d started.', PIDX, length(PARAMS)));
    stdout('Param = ');
    disp(param(PIDX));
    stdout('lsqnonlin_param = ');
    disp(param(PIDX).lsqnonlin_param);
    
    %% Load all the data
    data = load(DataFileName, '-mat');
    layout = load(LayoutFileName, '-mat');
    
    %% Make a list of good channels and their coordinates       
    [GoodMnt2D, GoodChannels] = load_mnt_channels_bbci (data.channels.Channel, layout.layout, SkipChannels);
    SensorsNum = sum(GoodChannels);
    
    %% Extract the EEG signals from the good channels
    data_brainstorm = data.Raw_1;
    %data_brainstorm = data.raw_data;
    elec = data_brainstorm.F (GoodChannels,:)';    
    Discr = 1 / data_brainstorm.Time(2);
    param(PIDX).Discr = Discr;
    T = size(elec,1);
    
    %% Preprocess the raw data
    [butter_preproc_b, butter_preproc_a] = butter(2,[fr_cutoff_low fr_cutoff_high]/(Discr/2));
    elec = filtfilt (butter_preproc_b, butter_preproc_a, elec);
    
    %% Prepare epochs info
    if epochs_load_on
        [epochs,~] = mk_epochs (data_brainstorm, 0);
        stdout('Epochs data loaded:');
    elseif ~isempty(epochs_default)
        stdout('Using default epochs:');
        epochs = epochs_default;
    else
        stdout('Epochs are off, using the whole time range:');
        epochs = [1 size(elec,1)];
    end
    
    if size(elec,1) < max(epochs(:))
        batcherr(sprintf('The signal is shorter than %d ticks.', max(epochs(:))));
        return;
    end
       
    epochs
    
    %% Hack
%     if epochs_on
%         epochs = [1 25000; 100000 125000];
%     else
%         epochs = [1 T];
%     end

    %% Make Butterworth filters
    [butter_base_b, butter_base_a, butter_p_b, butter_p_a, butter_q_b, butter_q_a, butter_pq_b, butter_pq_a] = mk_bp_filters ( FrP, FrQ, FrBase, Discr );
    
    %% Main function - 1st call
    [fig_idx] = gcfd_analyze ( elec, epochs, butter_p_b, butter_p_a, butter_q_b, butter_q_a, GoodMnt2D, param(PIDX), ...
        fig_handles, fig_idx, plotting_on );
    
    
    
    %%
        
%         %% Make up or load the electrode recordings
%         [elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
%             butter_p_b, butter_p_a, butter_q_b, butter_q_a] = mk_elec (source_analysis, GoodMnt2D, param(PIDX));
%         
%         %% Now we attempt to decompose them back
%         [PRE_1_ssd_primary(TIDX,:), PRE_1_ssd_secondary(TIDX,:), PRE_lin(TIDX,:), PRE_1_final(TIDX,:), fig_idx] = ...
%              gcfd_test ( elec, src_sig_p, src_sig_q, pattern_sig_p, pattern_sig_q, ...
%              butter_p_b, butter_p_a, butter_q_b, butter_q_a, ...
%              source_analysis, GoodMnt2D, param(PIDX), ...
%              fig_handles, fig_idx, plotting_on );        
%         
%         if xtest_pq            
%             batchout('2nd CROSS-TEST RUN, p:q switched to q:p:');
%             %% Now we attempt to decompose them back for the SECOND time, p:q switched to q:p:  
%             [param(PIDX).FrP, param(PIDX).FrQ] = deal (param(PIDX).FrQ, param(PIDX).FrP);
%             
%             [PRE_2_ssd_primary(TIDX,:), PRE_2_ssd_secondary(TIDX,:), PRE_2_final(TIDX,:), fig_idx] = ...
%                 gcfd_test ( elec, src_sig_q, src_sig_p, pattern_sig_q, pattern_sig_p, ...
%                 butter_q_b, butter_q_a, butter_p_b, butter_p_a, ...
%                 source_analysis, GoodMnt2D, param(PIDX), ...
%                 fig_handles, fig_idx, plotting_on );            
%             
%             if (FwdModel == FwdModelType.GuidoMNI) || (FwdModel == FwdModelType.Rand)
%                 disp('==================================================');
%                 fprintf('Pattern Recovery Error for P:Q=%d:%d\n\n',FrP,FrQ);
%                 %fprintf('P error (1st SSD only) (mean = %.4f)\n',mean(PRE_1_ssd_primary(TIDX,:)));
%                 %disp(PRE_1_ssd_primary(TIDX,:));
%                 fprintf('Q error ((2nd SSD)+gCFD) (mean = %.4f)\n',mean(PRE_1_final(TIDX,:)));
%                 disp(PRE_1_final(TIDX,:));
%                 disp('==================================================');
%                 fprintf('Pattern Recovery Error for Q:P=%d:%d\n\n',FrQ,FrP);
%                 fprintf('P error ((2nd SSD)+gCFD) (mean = %.4f)\n',mean(PRE_2_final(TIDX,:)));
%                 disp(PRE_2_final(TIDX,:));
%                 %fprintf('Q error (1st SSD only) (mean = %.4f)\n',mean(PRE_2_ssd_primary(TIDX,:)));
%                 %disp(PRE_2_ssd_primary(TIDX,:));
%                 disp('==================================================');
%                 fprintf('PRE Grand Mean ((2nd SSD)+gCFD) = %.4f\n',mean([PRE_1_final(TIDX,:) PRE_2_final(TIDX,:)]));
%                 disp('==================================================');
%             end
%         end
               
        batchout('Analysis completed.');

    
%     rng_saved_states = rng_states;
%     save([output_dir sprintf('[P=%d FrP=%d FrQ=%d] rng_saved_states',PIDX,FrP,FrQ)], 'rng_saved_states');
%     clearvars 'rng_saved_states';

    
    %%
    pidx = pidx+1;
end